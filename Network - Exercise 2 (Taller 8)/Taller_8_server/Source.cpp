#include <SFML\Graphics.hpp>
#include <SFML\Network.hpp>
#include <iostream>
#include <vector>
#include "InputMemoryBitStream.h"
#include "InputMemoryStream.h"
#include "OutputMemoryBitStream.h"
#include "OutputMemoryStream.h"
#include <cstdint>

#define PACKET_BITS 3
#define PLAYER_ID_BITS 3
#define GRID_X_BITS 10
#define GRID_Y_BITS 10
#define MAX_PACKETS_VECTOR_SIZE 32
#define PACKET_ID_BITS 5
#define WINDOWS_X 800
#define WINDOWS_Y 600
#define DELTA_X_BITS 6
#define DELTA_Y_BITS 6

using namespace std;

bool shouldclose = false;

enum PacketType {
	PT_EMPTY = 0,
	HELLO,
	ACK,
	WELCOME,
	PING,
	NEWPLAYER,
	REMOVEPLAYER,
	MOVEPLAYER
};


class timer {
private:
	unsigned long begTime;
public:
	void start() {
		begTime = clock();
	}

	unsigned long elapsedTime() {
		return ((unsigned long)clock() - begTime) / CLOCKS_PER_SEC;
	}

	bool isTimeout(unsigned long seconds) {
		return seconds >= elapsedTime();
	}
};


//Network manager class with UDP sockets. 
//Receive returns a simbs containing the contents of receiving using a certain socket from a certain IP and port.
//Send sends a certain ombs using a certain udp socket to a specific IP and port.
class NetworkManager {
private:

public:
	InputMemoryBitStream receive_data(sf::UdpSocket* socket_receive, sf::IpAddress* rIp, unsigned short* port)
	{
		std::size_t size;
		char buffer[1000];
		sf::Socket::Status status_server = socket_receive->receive(buffer, 1000, size, *rIp, *port);


		if (status_server == sf::Socket::NotReady)
		{
			//continue;
		}

		else if (status_server == sf::Socket::Done)
		{
			InputMemoryBitStream imbs(buffer, size * 8);
			return imbs;
		}
		else if (status_server == sf::Socket::Disconnected)
		{
			std::cout << "There was a disconnection." << std::endl;
		}
	}

	void send_data(sf::UdpSocket* socket_send, OutputMemoryBitStream& _ombs, unsigned short port)
	{
		sf::IpAddress sIp = sf::IpAddress::getLocalAddress();
		if (socket_send->send(_ombs.GetBufferPtr(), _ombs.GetByteLength(), sIp, port) != sf::Socket::Done)
		{
			std::cout << "There was an error while sending" << std::endl;
		}
	}

	void send_data(sf::UdpSocket* socket_send, std::string message, unsigned short port)
	{
		sf::IpAddress sIp = sf::IpAddress::getLocalAddress();
		char buffer[1000];
		strncpy_s(buffer, message.c_str(), sizeof(buffer));
		buffer[sizeof(buffer) - 1] = 0;
		if (socket_send->send(buffer, sizeof(buffer), sIp, port) != sf::Socket::Done)
		{
			std::cout << "There was an error while sending" << std::endl;
		}
	}
};

class Player
{
private:

public:
	int id;
	int position_x;
	int position_y;
	sf::IpAddress ip;
	unsigned short port;
	std::vector <unsigned short> messages_no_ack;
};

class MovementMessage
{

private:

public:
	int original_sender_id;
	int packet_id;
	std::string ack_message;
	std::string other_message;

	MovementMessage(int _original_sender_id, int _packet_id, std::string _ack_message, std::string _other_message)
	{
		original_sender_id = _original_sender_id;
		packet_id = _packet_id;
		ack_message = _ack_message;
		other_message = _other_message;
	}

	MovementMessage()
	{
		original_sender_id = 0;
		packet_id = 0;
		ack_message = "";
		other_message = "";
	}
};


int main()
{
	//sf::Vector2f grid[10][10];
	sf::UdpSocket socket_server;
	std::vector<Player> player_list;
	std::string PacketsSent[MAX_PACKETS_VECTOR_SIZE];
	std::vector<MovementMessage> MovementMessages;
	int16_t Packet_ID = -1;
	NetworkManager networkmanager;
	int nextplayerid = 0;

	sf::Socket::Status status_server = socket_server.bind(5000);

	if (status_server != sf::Socket::Done)
	{
		std::cout << "Error when binding socket to port 5000";
	}

	socket_server.setBlocking(false);

	timer no_ack_timer;
	no_ack_timer.start();
	timer ping_timer;
	ping_timer.start();
	timer movement_timer;
	movement_timer.start();

	while (true)
	{

		//Code to close the server when we need to
		if (shouldclose == true)
		{
			break;
		}

		//We receive
		sf::IpAddress rIp;
		unsigned short port;

		InputMemoryBitStream imbs = networkmanager.receive_data(&socket_server, &rIp, &port);

		PacketType pt = PacketType::PT_EMPTY;
		if (imbs.GetRemainingBitCount() > 0)
			imbs.Read(&pt, PACKET_BITS);
		if (pt == PacketType::ACK)
		{
			int k = 0;
			imbs.Read(&k, PACKET_ID_BITS);

			for (int i = 0; i < player_list.size(); i++)
			{
				if (player_list[i].port == port)
				{
					for (int j = 0; j < player_list[i].messages_no_ack.size(); j++)
					{
						if (k == player_list[i].messages_no_ack[j])
						{
							player_list[i].messages_no_ack.erase(player_list[i].messages_no_ack.begin() + j);
							break;
						}
					}
					break;
				}
			}
		}


		else if (pt == PacketType::HELLO)
		{
			bool old_player = false;
			for (int i = 0; i < player_list.size(); i++)
			{
				if (port == player_list[i].port)
				{
					OutputMemoryBitStream ombs;
					ombs.Write(PacketType::WELCOME, PACKET_BITS);
					ombs.Write(player_list[i].id, PLAYER_ID_BITS);
					ombs.Write((int16_t)player_list[i].position_x, GRID_X_BITS);
					ombs.Write((int16_t)player_list[i].position_y, GRID_Y_BITS);
					networkmanager.send_data(&socket_server, ombs, 5000);
					old_player = true;
					break;
				}
			}
			if (old_player == false)
			{
				Player auxiliar_player;
				nextplayerid++;
				auxiliar_player.id = nextplayerid;
				auxiliar_player.ip = rIp;
				auxiliar_player.port = port;
				int seed = static_cast<int>(time(0));
				srand(seed);
				int16_t initial_x = rand() % WINDOWS_X - 1;
				int16_t initial_y = rand() % WINDOWS_Y - 1;

				for (int i = 0; i < player_list.size(); i++)
				{
					if (initial_x + initial_y == player_list[i].position_x + player_list[i].position_y)
					{
						initial_x = rand() % WINDOWS_X - 1;
						initial_y = rand() % WINDOWS_Y - 1;
						i = -1;
					}
				}
				auxiliar_player.position_x = initial_x;
				auxiliar_player.position_y = initial_y;

				OutputMemoryBitStream ombs;
				ombs.Write(PacketType::WELCOME, PACKET_BITS);
				ombs.Write(nextplayerid, PLAYER_ID_BITS);
				ombs.Write((int16_t)auxiliar_player.position_x, GRID_X_BITS);
				ombs.Write((int16_t)auxiliar_player.position_y, GRID_Y_BITS);
				networkmanager.send_data(&socket_server, ombs, port);
				player_list.push_back(auxiliar_player);





				for (int i = 0; i < player_list.size() - 1; i++)
				{
					//std::string mensaje_2 = "NEWPLAYER_" + std::to_string(player_list[i].position_x) + ":" + std::to_string(player_list[i].position_y);
					int x = player_list[i].position_x;
					int  y = player_list[i].position_y;
					Packet_ID++;
					if (Packet_ID >= MAX_PACKETS_VECTOR_SIZE - 2)
						Packet_ID = 0;
					int k = Packet_ID;
					int16_t h = player_list[i].id;

					OutputMemoryBitStream ombs;
					ombs.Write(PacketType::NEWPLAYER, PACKET_BITS);
					ombs.Write(x, GRID_X_BITS);
					ombs.Write(y, GRID_Y_BITS);
					ombs.Write(k, PACKET_ID_BITS);
					ombs.Write(h, PLAYER_ID_BITS);

					networkmanager.send_data(&socket_server, ombs, port);
					player_list[player_list.size() - 1].messages_no_ack.push_back(Packet_ID);
					PacketsSent[Packet_ID] = std::string(ombs.GetBufferPtr());

				}

				Packet_ID++;
				if (Packet_ID >= MAX_PACKETS_VECTOR_SIZE - 2)
					Packet_ID = 0;
				int k = Packet_ID;
				int h = auxiliar_player.id;
				for (int i = 0; i < player_list.size() - 1; i++)
				{
					//std::string auxiliar_message = "NEWPLAYER_" + vector_string1 + ":" + vector_string2 + ";";
					OutputMemoryBitStream ombs;
					ombs.Write(PacketType::NEWPLAYER, PACKET_BITS);
					ombs.Write((int16_t)auxiliar_player.position_x, GRID_X_BITS);
					ombs.Write((int16_t)auxiliar_player.position_y, GRID_Y_BITS);
					ombs.Write(k, PACKET_ID_BITS);
					ombs.Write(h, PLAYER_ID_BITS);

					networkmanager.send_data(&socket_server, ombs, player_list[i].port);
					player_list[i].messages_no_ack.push_back(Packet_ID);
					PacketsSent[Packet_ID] = std::string(ombs.GetBufferPtr());
				}
			}
		}

		else if (pt == PacketType::MOVEPLAYER)
		{
			int16_t message_Packet_ID = 0;
			int16_t player_id = 0;
			int delta_x = 0;
			int delta_y = 0;
			int final_pos_x = 0;
			int final_pos_y = 0;
			bool signx;
			bool signy;
			if (imbs.GetRemainingBitCount() > 0)
				imbs.Read(&message_Packet_ID, PACKET_ID_BITS);

			if (imbs.GetRemainingBitCount() > 0)
				imbs.Read(&player_id, PLAYER_ID_BITS);

			if (imbs.GetRemainingBitCount() > 0)
				imbs.Read(&signx, 1);
			imbs.Read(&delta_x, DELTA_X_BITS);

			if (imbs.GetRemainingBitCount() > 0)
				imbs.Read(&signy, 1);
			imbs.Read(&delta_y, DELTA_Y_BITS);
			//imbs.Read(&final_pos_x, GRID_X_BITS);
			//imbs.Read(&final_pos_y, GRID_Y_BITS);

			if (signx == true)
			{
				delta_x = -delta_x;
			}

			if (signy == true)
			{
				delta_y = -delta_y;
			}

			for (int i = 0; i < player_list.size(); i++)
			{
				if (player_list[i].id == player_id)
				{
					final_pos_x = player_list[i].position_x + delta_x;
					final_pos_y = player_list[i].position_y + delta_y;

					player_list[i].position_x = final_pos_x;
					player_list[i].position_y = final_pos_y;

				}
			}

			OutputMemoryBitStream ombs;
			ombs.Write(ACK, PACKET_BITS);
			ombs.Write(MOVEPLAYER, PACKET_BITS);
			ombs.Write(message_Packet_ID, PACKET_ID_BITS);
			ombs.Write(final_pos_x, GRID_X_BITS);
			ombs.Write(final_pos_y, GRID_Y_BITS);

			Packet_ID++;
			if (Packet_ID >= MAX_PACKETS_VECTOR_SIZE - 2)
				Packet_ID = 0;

			OutputMemoryBitStream ombs_2;
			ombs_2.Write(MOVEPLAYER, PACKET_BITS);
			ombs_2.Write(player_id, PLAYER_ID_BITS);
			ombs_2.Write(final_pos_x, GRID_X_BITS);
			ombs_2.Write(final_pos_y, GRID_Y_BITS);
			ombs_2.Write(Packet_ID, PACKET_ID_BITS);

			MovementMessage movement_message(player_id, Packet_ID, ombs.GetBufferPtr(), ombs_2.GetBufferPtr());
			/*movement_message.ack_message = ombs.GetBufferPtr();
			movement_message.other_message = ombs_2.GetBufferPtr();
			movement_message.original_sender_id = player_id;
			movement_message.packet_id = Packet_ID;
			PacketsSent[Packet_ID] = movement_message.other_message;*/
			MovementMessages.push_back(movement_message);
		}


		//We send all no ack messages
		if (no_ack_timer.elapsedTime() > 1.0f)
		{
			no_ack_timer.start();
			for (int i = 0; i < player_list.size(); i++)
			{
				for (int j = 0; j < player_list[i].messages_no_ack.size(); j++)
				{
					networkmanager.send_data(&socket_server, PacketsSent[j], player_list[i].port);
				}
			}
		}


		//We send all ping messages and disconnect people who haven't answered
		if (ping_timer.elapsedTime() > 2.0f)
		{
			for (int i = 0; i < player_list.size(); i++)
			{
				for (int j = 0; j < player_list[i].messages_no_ack.size(); j++)
				{
					size_t auxiliar_size = PacketsSent[player_list[i].messages_no_ack[j]].length();
					char buffer[1000];
					strncpy_s(buffer, PacketsSent[player_list[i].messages_no_ack[j]].c_str(), sizeof(buffer));
					InputMemoryBitStream imbs(buffer, auxiliar_size);
					PacketType pt = PacketType::PT_EMPTY;
					if (imbs.GetRemainingBitCount() > 0)
						imbs.Read(&pt, PACKET_BITS);
					if (pt == PacketType::PING)
					{
						//std::string mensaje_2 = "REMOVEPLAYER_" + std::to_string(player_list[i].position_x) + ":" + std::to_string(player_list[i].position_y);
						Packet_ID++;
						if (Packet_ID >= MAX_PACKETS_VECTOR_SIZE - 2)
							Packet_ID = 0;
						int k = Packet_ID;
						int h = player_list[i].id;
						OutputMemoryBitStream ombs;
						ombs.Write(PacketType::REMOVEPLAYER, PACKET_BITS);
						ombs.Write(h, PLAYER_ID_BITS);
						ombs.Write(k, PACKET_ID_BITS);

						player_list.erase(player_list.begin() + i);
						for (int l = 0; l < player_list.size(); l++)
						{
							networkmanager.send_data(&socket_server, ombs, player_list[l].port);
							player_list[l].messages_no_ack.push_back(Packet_ID);
							PacketsSent[Packet_ID] = std::string(ombs.GetBufferPtr());
						}
						i--;
						break;
					}
				}
			}
			Packet_ID++;
			if (Packet_ID >= MAX_PACKETS_VECTOR_SIZE - 2)
				Packet_ID = 0;
			int k = Packet_ID;

			for (int i = 0; i < player_list.size(); i++)
			{
				OutputMemoryBitStream ombs;
				ombs.Write(PacketType::PING, PACKET_BITS);
				ombs.Write(k, PACKET_ID_BITS);
				networkmanager.send_data(&socket_server, ombs, player_list[i].port);
				player_list[i].messages_no_ack.push_back(Packet_ID);
				PacketsSent[Packet_ID] = std::string(ombs.GetBufferPtr());
				/*InputMemoryBitStream imbs(PacketsSent[player_list[i].messages_no_ack[player_list[i].messages_no_ack.size()-1]].GetBufferPtr(), PacketsSent[player_list[i].messages_no_ack[player_list[i].messages_no_ack.size() - 1]].GetByteLength());
				PacketType pt = PacketType::PT_EMPTY;
				if (imbs.GetRemainingBitCount() > 0)
				imbs.Read(&pt, 3);*/
			}
			ping_timer.start();
		}



		//We send movement messages
		if (movement_timer.elapsedTime() > 0.2f)
		{
			Player original_sender;

			//For each message we identify the original sender, send them the ack message and then send the other mesasge to all other players
			for (int i = 0; i < MovementMessages.size(); i++)
			{
				for (int j = 0; j < player_list.size(); j++)
				{
					if (MovementMessages[i].original_sender_id == player_list[j].id)
					{
						original_sender = player_list[j];
						networkmanager.send_data(&socket_server, MovementMessages[i].ack_message, original_sender.port);
						break;
					}
				}


				for (int j = 0; j < player_list.size(); j++)
				{
					if (original_sender.id != player_list[j].id)
					{
						networkmanager.send_data(&socket_server, MovementMessages[i].other_message, player_list[j].port);
						player_list[j].messages_no_ack.push_back(MovementMessages[i].packet_id);
						//PacketsSent[Packet_ID] = MovementMessage
					}
				}
			}
			MovementMessages.clear();
		}
	}
}