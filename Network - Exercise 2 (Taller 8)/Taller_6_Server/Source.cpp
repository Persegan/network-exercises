#include <SFML\Graphics.hpp>
#include <SFML\Network.hpp>
#include <iostream>
#include <vector>
#include "InputMemoryBitStream.h"
#include "InputMemoryStream.h"
#include "OutputMemoryBitStream.h"
#include "OutputMemoryStream.h"
#include <cstdint>

#define PACKET_BITS 3
#define PLAYER_ID_BITS 5
#define GRID_X_BITS 4
#define GRID_Y_BITS 4
#define MAX_PACKETS_VECTOR_SIZE 120
#define PACKET_ID_BITS 7

using namespace std;

bool shouldclose = false;

enum PacketType {
	PT_EMPTY = 0,
	HELLO,
	ACK,
	WELCOME,
	PING,
	NEWPLAYER,
	REMOVEPLAYER
};


class timer {
private:
	unsigned long begTime;
public:
	void start() {
		begTime = clock();
	}

	unsigned long elapsedTime() {
		return ((unsigned long)clock() - begTime) / CLOCKS_PER_SEC;
	}

	bool isTimeout(unsigned long seconds) {
		return seconds >= elapsedTime();
	}
};


//Network manager class with UDP sockets. 
//Receive returns a simbs containing the contents of receiving using a certain socket from a certain IP and port.
//Send sends a certain ombs using a certain udp socket to a specific IP and port.
class NetworkManager {
private:

public:
	InputMemoryBitStream receive_data(sf::UdpSocket* socket_receive, sf::IpAddress* rIp, unsigned short* port)
	{
		std::size_t size;
		char buffer[1000];
		sf::Socket::Status status_server = socket_receive->receive(buffer, 1000, size, *rIp, *port);

		
		if (status_server == sf::Socket::NotReady)
		{
			//continue;
		}

		else if (status_server == sf::Socket::Done)
		{
			InputMemoryBitStream imbs(buffer, size * 8);
			return imbs;
		}
		else if (status_server == sf::Socket::Disconnected)
		{
			std::cout << "There was a disconnection." << std::endl;
		}
	}

	void send_data(sf::UdpSocket* socket_send, OutputMemoryBitStream& _ombs, unsigned short port)
	{
		sf::IpAddress sIp = sf::IpAddress::getLocalAddress();
		if (socket_send->send(_ombs.GetBufferPtr(), _ombs.GetByteLength(), sIp, port) != sf::Socket::Done)
		{
			std::cout << "There was an error while sending" << std::endl;
		}
	}

	void send_data(sf::UdpSocket* socket_send, std::string message, unsigned short port)
	{
		sf::IpAddress sIp = sf::IpAddress::getLocalAddress();
		char buffer[1000];
		strncpy_s(buffer, message.c_str(), sizeof(buffer));
		buffer[sizeof(buffer) - 1] = 0;
		if (socket_send->send(buffer, sizeof(buffer), sIp, port) != sf::Socket::Done)
		{
			std::cout << "There was an error while sending" << std::endl;
		}
	}
};

class Player
{
private:

public:
	int id;
	int position_x;
	int position_y;
	sf::IpAddress ip;
	unsigned short port;
	std::vector <unsigned short> messages_no_ack;
};



int main()
{
	sf::Vector2f grid[10][10];
	sf::UdpSocket socket_server;
	std::vector<Player> player_list;
	std::string PacketsSent[MAX_PACKETS_VECTOR_SIZE];
	int Packet_ID = -1;
	NetworkManager networkmanager;
	int nextplayerid = 0;

	sf::Socket::Status status_server = socket_server.bind(5000);

	if (status_server != sf::Socket::Done)
	{
		std::cout << "Error when binding socket to port 5000";
	}
	
	socket_server.setBlocking(false);

	timer no_ack_timer;
	no_ack_timer.start();
	timer ping_timer;
	ping_timer.start();
	
	while (true)
	{
		
		//Code to close the server when we need to
		if (shouldclose == true)
		{
			break;
		}

		//We receive
		sf::IpAddress rIp;
		unsigned short port;
		
		InputMemoryBitStream imbs = networkmanager.receive_data(&socket_server, &rIp, &port);

		PacketType pt = PacketType::PT_EMPTY;
		if (imbs.GetRemainingBitCount() > 0)
			imbs.Read(&pt, PACKET_BITS);
		if (pt == PacketType::ACK)
		{
			int k = 0;
			imbs.Read(&k, PACKET_ID_BITS);

			for (int i = 0; i < player_list.size(); i++)
			{
				if (player_list[i].port == port)
				{
					for (int j = 0; j < player_list[i].messages_no_ack.size(); j++)
					{
						if (k == player_list[i].messages_no_ack[j])
						{
							player_list[i].messages_no_ack.erase(player_list[i].messages_no_ack.begin() + j);
							break;
						}
					}
					break;
				}
			}
		}


		else if (pt == PacketType::HELLO)
		{
			bool old_player = false;
			for (int i = 0; i < player_list.size(); i++)
			{
				if (port == player_list[i].port)
				{
					OutputMemoryBitStream ombs;
					ombs.Write(PacketType::WELCOME, PACKET_BITS);
					ombs.Write(player_list[i].id, PLAYER_ID_BITS);
					ombs.Write((int8_t)player_list[i].position_x, GRID_X_BITS);
					ombs.Write((int8_t)player_list[i].position_y, GRID_Y_BITS);
					networkmanager.send_data(&socket_server, ombs, 5000);
					old_player = true;
					break;
				}
			}
			if (old_player == false)
			{
				Player auxiliar_player;
				nextplayerid++;
				auxiliar_player.id = nextplayerid;
				auxiliar_player.ip = rIp;
				auxiliar_player.port = port;
				int seed = static_cast<int>(time(0));
				srand(seed);
				int8_t initial_x = rand() % 10;
				int8_t initial_y = rand() % 10;

				for (int i = 0; i < player_list.size(); i++)
				{
					if (initial_x + initial_y == player_list[i].position_x + player_list[i].position_y)
					{
						initial_x = rand() % 10;
						initial_y = rand() % 10;
						i = -1;
					}
				}
				auxiliar_player.position_x = initial_x;
				auxiliar_player.position_y = initial_y;

				OutputMemoryBitStream ombs;
				ombs.Write(PacketType::WELCOME, PACKET_BITS);
				ombs.Write(nextplayerid, PLAYER_ID_BITS);
				ombs.Write((int8_t) auxiliar_player.position_x, GRID_X_BITS);
				ombs.Write((int8_t)auxiliar_player.position_y, GRID_Y_BITS);
				networkmanager.send_data(&socket_server, ombs, port);
				player_list.push_back(auxiliar_player);

				



				for (int i = 0; i < player_list.size()-1; i++)
				{
					//std::string mensaje_2 = "NEWPLAYER_" + std::to_string(player_list[i].position_x) + ":" + std::to_string(player_list[i].position_y);
					int8_t  x = player_list[i].position_x;
					int8_t  y = player_list[i].position_y;
					Packet_ID++;
					if (Packet_ID >= MAX_PACKETS_VECTOR_SIZE - 2)
						Packet_ID = 0;
					int k = Packet_ID;

					OutputMemoryBitStream ombs;
					ombs.Write(PacketType::NEWPLAYER, PACKET_BITS);
					ombs.Write(x, GRID_X_BITS);
					ombs.Write(y, GRID_Y_BITS);
					ombs.Write(k, PACKET_ID_BITS);

					networkmanager.send_data(&socket_server, ombs, port);
					player_list[player_list.size() - 1].messages_no_ack.push_back(Packet_ID);
					PacketsSent[Packet_ID] = std::string(ombs.GetBufferPtr());

				}

				Packet_ID++;
				if (Packet_ID >= MAX_PACKETS_VECTOR_SIZE - 2)
					Packet_ID = 0;
				int k = Packet_ID;
				for (int i = 0; i < player_list.size()-1; i++)
				{
					//std::string auxiliar_message = "NEWPLAYER_" + vector_string1 + ":" + vector_string2 + ";";
					OutputMemoryBitStream ombs;
					ombs.Write(PacketType::NEWPLAYER, PACKET_BITS);
					ombs.Write((int8_t)auxiliar_player.position_x, GRID_X_BITS);
					ombs.Write((int8_t)auxiliar_player.position_y, GRID_Y_BITS);
					ombs.Write(k, PACKET_ID_BITS);

					networkmanager.send_data(&socket_server, ombs, player_list[i].port);
					player_list[i].messages_no_ack.push_back(Packet_ID);
					PacketsSent[Packet_ID] = std::string(ombs.GetBufferPtr());
				}
			}
		}

		
		//We send all no ack messages
		if (no_ack_timer.elapsedTime() > 1.0f)
		{
			no_ack_timer.start();
			for (int i = 0; i < player_list.size(); i++)
			{
				for (int j = 0; j < player_list[i].messages_no_ack.size(); j++)
				{
					networkmanager.send_data(&socket_server, PacketsSent[j], player_list[i].port);
				}
			}
		}

		
		//We send all ping messages and disconnect people who haven't answered
		if (ping_timer.elapsedTime() > 2.0f)
		{
			for (int i = 0; i < player_list.size(); i++)
			{
				for (int j = 0; j < player_list[i].messages_no_ack.size(); j++)
				{
					size_t auxiliar_size = PacketsSent[player_list[i].messages_no_ack[j]].length();
					char buffer[1000];
					strncpy_s(buffer, PacketsSent[player_list[i].messages_no_ack[j]].c_str(), sizeof(buffer));
					InputMemoryBitStream imbs(buffer, auxiliar_size);
					PacketType pt = PacketType::PT_EMPTY;
					if (imbs.GetRemainingBitCount() > 0)
						imbs.Read(&pt, PACKET_BITS);
					if (pt == PacketType::PING)
					{
						//std::string mensaje_2 = "REMOVEPLAYER_" + std::to_string(player_list[i].position_x) + ":" + std::to_string(player_list[i].position_y);
						Packet_ID++;
						if (Packet_ID >= MAX_PACKETS_VECTOR_SIZE - 2)
							Packet_ID = 0;
						int k = Packet_ID;
						OutputMemoryBitStream ombs;
						ombs.Write(PacketType::REMOVEPLAYER, PACKET_BITS);
						ombs.Write((int8_t)player_list[i].position_x, GRID_X_BITS);
						ombs.Write((int8_t)player_list[i].position_y, GRID_Y_BITS);
						ombs.Write(k, PACKET_ID_BITS);

						player_list.erase(player_list.begin() + i);
						for (int l = 0; l < player_list.size(); l++)
						{
							networkmanager.send_data(&socket_server, ombs, player_list[l].port);
							player_list[l].messages_no_ack.push_back(Packet_ID);
							PacketsSent[Packet_ID] = std::string(ombs.GetBufferPtr());
						}
						i--;
					}
				}
			}
			Packet_ID++;
			if (Packet_ID >= MAX_PACKETS_VECTOR_SIZE - 2)
				Packet_ID = 0;
			int k = Packet_ID;

			for (int i = 0; i < player_list.size(); i++)
			{
				OutputMemoryBitStream ombs;
				ombs.Write(PacketType::PING, PACKET_BITS);
				ombs.Write(k, PACKET_ID_BITS);
				networkmanager.send_data(&socket_server, ombs, player_list[i].port);
				player_list[i].messages_no_ack.push_back(Packet_ID);
				PacketsSent[Packet_ID] = std::string(ombs.GetBufferPtr());
				/*InputMemoryBitStream imbs(PacketsSent[player_list[i].messages_no_ack[player_list[i].messages_no_ack.size()-1]].GetBufferPtr(), PacketsSent[player_list[i].messages_no_ack[player_list[i].messages_no_ack.size() - 1]].GetByteLength());
				PacketType pt = PacketType::PT_EMPTY;
				if (imbs.GetRemainingBitCount() > 0)
					imbs.Read(&pt, 3);*/
			}
			ping_timer.start();
		}
	}
}