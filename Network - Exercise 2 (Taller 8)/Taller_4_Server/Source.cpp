﻿#include <SFML\Graphics.hpp>
#include <SFML\Network.hpp>
#include <string>
#include <iostream>
#include <vector>


bool shouldclose = false;

class timer {
private:
	unsigned long begTime;
public:
	void start() {
		begTime = clock();
	}

	unsigned long elapsedTime() {
		return ((unsigned long)clock() - begTime) / CLOCKS_PER_SEC;
	}

	bool isTimeout(unsigned long seconds) {
		return seconds >= elapsedTime();
	}
};


class NetworkManager {
private:

public:
	void receive_data(std::vector<std::string>* aMensajes, sf::TcpSocket* socket_receive)
	{
		aMensajes->clear();
		char data[1000];
		size_t sizeRecv;
		sf::Socket::Status status_server;
		status_server = socket_receive->receive(data, 1000, sizeRecv);
		if (status_server == sf::Socket::NotReady)
		{
			//continue;
		}

		else if (status_server == sf::Socket::Done)
		{

			if (sizeRecv != 1000)
			{
				data[sizeRecv] = '\0';
			}

			sf::String mensaje = data;
			while (mensaje.find(';') != std::string::npos)
			{
				aMensajes->push_back(mensaje.substring(0, mensaje.find(';')+1));
				mensaje = mensaje.substring(mensaje.find(';') + 1, mensaje.find('\0') - mensaje.find(';') - 1);
			}
			/*for (int i = 0; i < aMensajes.size(); i++)
			{
			mensaje = aMensajes[i];
			sf::String auxiliar_mensaje = mensaje.substring(0, mensaje.find('_') - 1);
			}*/
		}
		else if (status_server == sf::Socket::Disconnected)
		{
			std::cout << "There was a disconnection. The game is now broken." << std::endl;
			socket_receive->disconnect();
			shouldclose = true;
		}
	}


	void send_data(std::string data, sf::TcpSocket* socket_send)
	{
		sf::Socket::Status status_server;
		size_t bytesSent;
		status_server = socket_send->send(data.c_str(), data.length(), bytesSent);

		while (status_server == sf::Socket::Status::Partial)
		{
			data = data.substr(bytesSent, data.length());
			status_server = socket_send->send(data.c_str(), data.length() - bytesSent, bytesSent);
		}

		if (status_server == sf::Socket::Status::Disconnected)
		{
			socket_send->disconnect();
			std::cout << "Player 1 disconnected" << std::endl;
			shouldclose = true;
		}
	}
};







using namespace std;

int main()
{
	NetworkManager network_manager;
	//socketsListenList.push_back();

	// .push_back
	// .erase
	sf::Socket::Status status_server;

	sf::TcpListener listener;
	//sf::TcpSocket* incoming = new sf::TcpSocket;
	//incoming->setBlocking(false);
	status_server = listener.listen(5000);

	std::string player1_name = "";
	std::string player2_name = "";

	std::vector<std::string> aMensajes;

	sf::String mensaje_player1 = "";
	sf::String mensaje_player2 = "";

	sf::String messagetoadd_player1 = "";
	sf::String messagetoadd_player2 = "";

	int score_player1 = 0;
	int score_player2 = 0;
	int round = 0;

	sf::TcpSocket socket_player1;
	sf::TcpSocket socket_player2;
	status_server = listener.accept(socket_player1);
	if (status_server != sf::Socket::Status::Done)
	{
		std::cout << "Error al escuchar por el puerto 5000." << std::endl;
	}
	status_server = listener.accept(socket_player2);
	if (status_server != sf::Socket::Status::Done)
	{
		std::cout << "Error al escuchar por el puerto 5000." << std::endl;
	}

	socket_player1.setBlocking(false);
	socket_player2.setBlocking(false);

	network_manager.send_data("GAMESTART_player1;", &socket_player1);
	network_manager.send_data("GAMESTART_player2;", &socket_player2);

	


	while (player1_name == "")
	{
		network_manager.receive_data(&aMensajes, &socket_player1);
		for (int i = 0; i < aMensajes.size(); i++)
		{
			sf::String mensaje = aMensajes[i];
			sf::String auxiliar_mensaje = mensaje.substring(0, mensaje.find('_'));
			std::string temporal = auxiliar_mensaje;
			if (auxiliar_mensaje == "PLAYER1NAME")
			{
				mensaje = mensaje.substring(mensaje.find('_') + 1, mensaje.find(';') - mensaje.find('_') - 1);
				player1_name = mensaje;
			}
		}
	}

	while (player2_name == "")
	{
		network_manager.receive_data(&aMensajes, &socket_player2);
		for (int i = 0; i < aMensajes.size(); i++)
		{
			sf::String mensaje = aMensajes[i];
			sf::String auxiliar_mensaje = mensaje.substring(0, mensaje.find('_'));
			if (auxiliar_mensaje == "PLAYER2NAME")
			{
				mensaje = mensaje.substring(mensaje.find('_') + 1, mensaje.find(';') - mensaje.find('_') - 1);
				player2_name = mensaje;
			}
		}
	}

	std::string auxiliar_mensaje = "PLAYER2NAME_" + player2_name + ";";
	network_manager.send_data(auxiliar_mensaje, &socket_player1);
	auxiliar_mensaje = "PLAYER1NAME_" + player1_name + ";";
	network_manager.send_data(auxiliar_mensaje, &socket_player2);


	//Inicializamos el timer. Hay una clase para timer
	unsigned long seconds = 20;
	auxiliar_mensaje = "TIMERSTART_" + std::to_string(seconds) + ";";
	network_manager.send_data(auxiliar_mensaje, &socket_player1);
	timer t;
	t.start();



	string word_to_type;
	int random = rand() % 5;
	switch (random)
	{
	case 0:
		word_to_type = "p1";
		break;

	case 1:
		word_to_type = "p2";
		break;

	case 2:
		word_to_type = "p3";
		break;

	case 3:
		word_to_type = "p4";
		break;

	case 4:
		word_to_type = "p5";
		break;

	default:

		break;
	}

	auxiliar_mensaje = "WORDTOTYPE_" + word_to_type + ";";
	network_manager.send_data(auxiliar_mensaje, &socket_player1);
	network_manager.send_data(auxiliar_mensaje, &socket_player2);


	while (true)
	{
		if (shouldclose == true)
		{
			break;
		}
		//Si entra aqui el tiempo se acabo, y cambiamos palabra
		if (t.elapsedTime() >= seconds)
		{
			if (score_player1 == 4 || score_player2 == 4)
			{
				if (score_player1 > score_player2)
				{

					auxiliar_mensaje = "GAMEEND_player1;";
					network_manager.send_data(auxiliar_mensaje, &socket_player1);
					network_manager.send_data(auxiliar_mensaje, &socket_player2);
				}
				else if (score_player1 < score_player2)
				{
					auxiliar_mensaje = "GAMEEND_player2;";
					network_manager.send_data(auxiliar_mensaje, &socket_player1);
					network_manager.send_data(auxiliar_mensaje, &socket_player2);
				}
				else if (score_player1 == score_player2)
				{
					auxiliar_mensaje = "GAMEEND_draw;";
					network_manager.send_data(auxiliar_mensaje, &socket_player1);
					network_manager.send_data(auxiliar_mensaje, &socket_player2);
				}
			}
			random = rand() % 5;
			switch (random)
			{
			case 0:
				word_to_type = "p1";
				break;

			case 1:
				word_to_type = "p2";
				break;

			case 2:
				word_to_type = "p3";
				break;

			case 3:
				word_to_type = "p4";
				break;

			case 4:
				word_to_type = "p5";
				break;

			default:

				break;
			}
			seconds = 20;
			auxiliar_mensaje = "TIMERSTART_" + std::to_string(seconds) + ";";
			network_manager.send_data(auxiliar_mensaje, &socket_player1);
			network_manager.send_data(auxiliar_mensaje, &socket_player2);
			t.start();
			auxiliar_mensaje = "WORDTOTYPE_" + word_to_type + ";";
			network_manager.send_data(auxiliar_mensaje, &socket_player1);
			network_manager.send_data(auxiliar_mensaje, &socket_player2);
		}
		//RECIBIMOS DE LOS DOS JUGADORES, PRIMERO 1 LUEGO EL OTRO
		for (int i = 0; i < 2; i++)
		{
			if (i == 0)
			{
				network_manager.receive_data(&aMensajes, &socket_player1);
			}
			else if (i == 1)
			{
				network_manager.receive_data(&aMensajes, &socket_player2);
			}

			for (int i = 0; i < aMensajes.size(); i++)
			{
				sf::String mensaje = aMensajes[i];
				sf::String auxiliar_mensaje = mensaje.substring(0, mensaje.find('_'));
				if (auxiliar_mensaje == "PLAYER1SEND")
				{
					mensaje = mensaje.substring(mensaje.find('_') + 1, mensaje.find(';') - mensaje.find('_') - 1);
					std::string potato = mensaje;
					if (mensaje == word_to_type)
					{
						if (score_player1 == 4||score_player2 == 4)
						{
							if (score_player1 > score_player2)
							{

								auxiliar_mensaje = "GAMEEND_player1;";
								network_manager.send_data(auxiliar_mensaje, &socket_player1);
								network_manager.send_data(auxiliar_mensaje, &socket_player2);
							}
							else if (score_player1 < score_player2)
							{
								auxiliar_mensaje = "GAMEEND_player2;";
								network_manager.send_data(auxiliar_mensaje, &socket_player1);
								network_manager.send_data(auxiliar_mensaje, &socket_player2);
							}
							else if (score_player1 == score_player2)
							{
								auxiliar_mensaje = "GAMEEND_draw;";
								network_manager.send_data(auxiliar_mensaje, &socket_player1);
								network_manager.send_data(auxiliar_mensaje, &socket_player2);
							}
						}
						sf::String potato = "correct";
						auxiliar_mensaje = "PLAYER1ADDTOLOG_" + potato + ";";
						network_manager.send_data(auxiliar_mensaje, &socket_player1);
						network_manager.send_data(auxiliar_mensaje, &socket_player2);
						score_player1++;
						auxiliar_mensaje = "SCORE1_" + std::to_string(score_player1) + ";";
						network_manager.send_data(auxiliar_mensaje, &socket_player1);
						network_manager.send_data(auxiliar_mensaje, &socket_player2);
						random = rand() % 5;
						switch (random)
						{
						case 0:
							word_to_type = "p1";
							break;

						case 1:
							word_to_type = "p2";
							break;

						case 2:
							word_to_type = "p3";
							break;

						case 3:
							word_to_type = "p4";
							break;

						case 4:
							word_to_type = "p5";
							break;

						default:

							break;
						}
						seconds = 20;
						auxiliar_mensaje = "TIMERSTART_" + std::to_string(seconds) + ";";
						network_manager.send_data(auxiliar_mensaje, &socket_player1);
						network_manager.send_data(auxiliar_mensaje, &socket_player2);
						t.start();
						auxiliar_mensaje = "WORDTOTYPE_" + word_to_type + ";";
						network_manager.send_data(auxiliar_mensaje, &socket_player1);
						network_manager.send_data(auxiliar_mensaje, &socket_player2);
					}
					else
					{
						sf::String potato = "incorrect";
						auxiliar_mensaje = "PLAYER1ADDTOLOG_" + potato + ";";
						network_manager.send_data(auxiliar_mensaje, &socket_player1);
						network_manager.send_data(auxiliar_mensaje, &socket_player2);
					}


				}
				else if (auxiliar_mensaje == "PLAYER2SEND")
				{
					mensaje = mensaje.substring(mensaje.find('_') + 1, mensaje.find(';') - mensaje.find('_') - 1);
					if (mensaje == word_to_type)
					{
						round++;
						if (round == 5)
						{
							if (score_player1 > score_player2)
							{

								auxiliar_mensaje = "GAMEEND_player1;";
								network_manager.send_data(auxiliar_mensaje, &socket_player1);
								network_manager.send_data(auxiliar_mensaje, &socket_player2);
							}
							else if (score_player1 < score_player2)
							{
								auxiliar_mensaje = "GAMEEND_player2;";
								network_manager.send_data(auxiliar_mensaje, &socket_player1);
								network_manager.send_data(auxiliar_mensaje, &socket_player2);
							}
							else if (score_player1 == score_player2)
							{
								auxiliar_mensaje = "GAMEEND_draw;";
								network_manager.send_data(auxiliar_mensaje, &socket_player1);
								network_manager.send_data(auxiliar_mensaje, &socket_player2);
							}
						}
						sf::String potato = "correct";
						auxiliar_mensaje = "PLAYER2ADDTOLOG_" + potato + ";";
						network_manager.send_data(auxiliar_mensaje, &socket_player1);
						network_manager.send_data(auxiliar_mensaje, &socket_player2);
						score_player2++;
						auxiliar_mensaje = "SCORE2_" + std::to_string(score_player2) + ";";
						network_manager.send_data(auxiliar_mensaje, &socket_player1);
						network_manager.send_data(auxiliar_mensaje, &socket_player2);
						random = rand() % 5;
						switch (random)
						{
						case 0:
							word_to_type = "p1";
							break;

						case 1:
							word_to_type = "p2";
							break;

						case 2:
							word_to_type = "p3";
							break;

						case 3:
							word_to_type = "p4";
							break;

						case 4:
							word_to_type = "p5";
							break;

						default:

							break;
						}
						seconds = 20;
						auxiliar_mensaje = "TIMERSTART_" + std::to_string(seconds) + ";";
						network_manager.send_data(auxiliar_mensaje, &socket_player1);
						network_manager.send_data(auxiliar_mensaje, &socket_player2);
						t.start();
						auxiliar_mensaje = "WORDTOTYPE_" + word_to_type + ";";
						network_manager.send_data(auxiliar_mensaje, &socket_player1);
						network_manager.send_data(auxiliar_mensaje, &socket_player2);
					}
					else
					{
						sf::String potato = "incorrect";
						auxiliar_mensaje = "PLAYER2ADDTOLOG_" + potato + ";";
						network_manager.send_data(auxiliar_mensaje, &socket_player1);
						network_manager.send_data(auxiliar_mensaje, &socket_player2);
					}
				}
				else if (auxiliar_mensaje == "PLAYER1ADDCHAR")
				{
					mensaje = mensaje.substring(mensaje.find('_') + 1, mensaje.find(';') - mensaje.find('_') - 1);
					auxiliar_mensaje = "PLAYER1ADDCHAR_" + mensaje + ";";
					network_manager.send_data(auxiliar_mensaje, &socket_player1);
					network_manager.send_data(auxiliar_mensaje, &socket_player2);
				}
				else if (auxiliar_mensaje == "PLAYER2ADDCHAR")
				{
					mensaje = mensaje.substring(mensaje.find('_') + 1, mensaje.find(';') - mensaje.find('_') - 1);
					auxiliar_mensaje = "PLAYER2ADDCHAR_" + mensaje + ";";
					network_manager.send_data(auxiliar_mensaje, &socket_player1);
					network_manager.send_data(auxiliar_mensaje, &socket_player2);
				}
				else if (auxiliar_mensaje == "PLAYER1REMOVECHAR")
				{
					auxiliar_mensaje = "PLAYER1REMOVECHAR_;";
					network_manager.send_data(auxiliar_mensaje, &socket_player1);
					network_manager.send_data(auxiliar_mensaje, &socket_player2);
				}
				else if (auxiliar_mensaje == "PLAYER2REMOVECHAR")
				{
					auxiliar_mensaje = "PLAYER2REMOVECHAR_;";
					network_manager.send_data(auxiliar_mensaje, &socket_player1);
					network_manager.send_data(auxiliar_mensaje, &socket_player2);
				}
				else if (auxiliar_mensaje == "PLAYER1CLEAR")
				{
					auxiliar_mensaje = "PLAYER1CLEAR_;";
					network_manager.send_data(auxiliar_mensaje, &socket_player1);
					network_manager.send_data(auxiliar_mensaje, &socket_player2);
				}
				else if (auxiliar_mensaje == "PLAYER2CLEAR")
				{
					auxiliar_mensaje = "PLAYER2CLEAR_;";
					network_manager.send_data(auxiliar_mensaje, &socket_player1);
					network_manager.send_data(auxiliar_mensaje, &socket_player2);
				}
			}
		}
	}
}


