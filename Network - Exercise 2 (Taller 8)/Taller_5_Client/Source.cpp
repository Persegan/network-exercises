#include <SFML\Graphics.hpp>
#include <SFML\Network.hpp>
#include <string>
#include <iostream>
#include <vector>



//#include <Mouse.hpp>

#define MAX_MENSAJES 30

bool shouldclose = false;


class timer {
private:
	unsigned long begTime;
public:
	void start() {
		begTime = clock();
	}

	unsigned long elapsedTime() {
		return ((unsigned long)clock() - begTime) / CLOCKS_PER_SEC;
	}

	bool isTimeout(unsigned long seconds) {
		return seconds >= elapsedTime();
	}
};

//Network manager class with UDP sockets. 
//Receive returns a std::string containing the contents of receiving using a certain socket from a certain IP and port.
//Send sends a certain string using a certain udp socket to a specific IP and port.
class NetworkManager {
private:

public:
	void receive_data(sf::UdpSocket* socket_receive, sf::IpAddress* rIp, unsigned short* port, sf::String* message, std::size_t* size)
	{
		char buffer[1000];
		sf::Socket::Status status_server = socket_receive->receive(buffer, sizeof(buffer), *size, *rIp, *port);

		if (status_server == sf::Socket::NotReady)
		{
			//continue;
		}

		else if (status_server == sf::Socket::Done)
		{
			*message = std::string(buffer);
		}
		else if (status_server == sf::Socket::Disconnected)
		{
			std::cout << "There was a disconnection." << std::endl;
		}
	}

	void send_data(sf::UdpSocket* socket_send, std::string message, unsigned short port)
	{
		sf::IpAddress sIp = sf::IpAddress::getLocalAddress();
		char buffer[1000];
		strncpy_s(buffer, message.c_str(), sizeof(buffer));
		buffer[sizeof(buffer) - 1] = 0;
		if (socket_send->send(buffer, sizeof(buffer), sIp, port) != sf::Socket::Done)
		{
			std::cout << "There was an error while sending" << std::endl;
		}
	}
};


int main()
{

#pragma region connection

	sf::Vector2f grid[10][10];
	for (int j = 0; j < 10; j++) {
		for (int i = 0; i < 10; i++) {
			grid[i][j] = sf::Vector2f(59.5f / 2 + 59.5f*i, 59.5f / 2 + 59.5f*j);

		}
	}
	int estadoGrid[10][10];
	//0 Nada
	//1 JugadorLocal
	//2 JugadorExterno
	for (int j = 0; j < 10; j++) {
		for (int i = 0; i < 10; i++) {
			estadoGrid[i][j] = 0;

		}
	}
	int seed = static_cast<int>(time(0));
	srand(seed);
	int initial_x = rand() % 10;
	int initial_y = rand() % 10;
	estadoGrid[initial_x][initial_y] = 1;
	sf::UdpSocket socket_client;
	NetworkManager networkmanager;
	int player_id = 0;
	timer timer1;
	socket_client.setBlocking(false);
#pragma endregion

	timer1.start();

	while (player_id == 0)
	{
		if (timer1.elapsedTime() > 0.4f)
		{
			std::string string_initial_x = std::to_string(initial_x);
			std::string string_initial_y = std::to_string(initial_y);
			std::string mensaje = "HELLO_" + string_initial_x + ":" + string_initial_y + ";";
			networkmanager.send_data(&socket_client, mensaje, 5000);
			timer1.start();
		}

		sf::String mensaje_2;
		sf::IpAddress rIp;
		unsigned short port;
		std::size_t size;
		networkmanager.receive_data(&socket_client, &rIp, &port, &mensaje_2, &size);

		sf::String auxiliar_mensaje = mensaje_2.substring(0, mensaje_2.find('_'));
		if (auxiliar_mensaje == "WELCOME")
		{
			std::string mensaje_3 = mensaje_2.substring(mensaje_2.find('_') + 1, mensaje_2.find(';') - mensaje_2.find('_') - 1);
			player_id = std::stoi(mensaje_3);
		}
	}

#pragma region chat

	sf::Vector2i screenDimensions(800, 600);

	sf::RenderWindow window;
	window.create(sf::VideoMode(screenDimensions.x, screenDimensions.y), "Game");

	sf::Font font;
	if (!font.loadFromFile("courbd.ttf"))
	{
		std::cout << "Can't load the font file" << std::endl;
	}





	sf::RectangleShape temporal(sf::Vector2f(600, 5));
	sf::RectangleShape Hseparators[11];
	for (int i = 0; i < 11; i++)
	{
		Hseparators[i] = temporal;

		Hseparators[i].setFillColor(sf::Color(100, 200, 200, 255));
		Hseparators[i].setPosition(0, 59.5f * i);
	}
	sf::RectangleShape temporal2(sf::Vector2f(5, 800));
	sf::RectangleShape Vseparators[11];
	for (int i = 0; i < 11; i++)
	{
		Vseparators[i] = temporal2;

		Vseparators[i].setFillColor(sf::Color(100, 200, 200, 255));
		Vseparators[i].setPosition(59.5f * i, 0);
	}


	while (true)
	{
		if (shouldclose == true)
		{
			window.close();
		}


		//Send test

		// We receive
		sf::String mensaje;
		sf::IpAddress rIp;
		unsigned short port;
		std::size_t size;

		networkmanager.receive_data(&socket_client, &rIp, &port, &mensaje, &size);

		sf::String auxiliar_mensaje = mensaje.substring(0, mensaje.find('_'));
		if (auxiliar_mensaje == "PING")
		{
			std::string mensaje_2 = "ACK_PING_;";
			networkmanager.send_data(&socket_client, mensaje_2, 5000);
		}

		else if (auxiliar_mensaje == "NEWPLAYER")
		{
			sf::String mensaje_2 = mensaje.substring(mensaje.find('_') + 1, mensaje.find(';') - mensaje.find('_') - 1);
			std::string vector_string1 = mensaje_2.substring(0, mensaje_2.find(':'));
			std::string vector_string2 = mensaje_2.substring(mensaje_2.find(':') + 1, mensaje_2.find(';') - mensaje_2.find(':') - 1);
			int i = std::stoi(vector_string1);
			int j = std::stoi(vector_string2);
			if (estadoGrid[i][j] == 0)
			{
				estadoGrid[i][j] = 2;
			}
			mensaje = "ACK_" + mensaje;
			networkmanager.send_data(&socket_client, mensaje, 5000);
		}

		else if (auxiliar_mensaje == "REMOVEPLAYER")
		{
			sf::String mensaje_2 = mensaje.substring(mensaje.find('_') + 1, mensaje.find(';') - mensaje.find('_') - 1);
			std::string vector_string1 = mensaje_2.substring(0, mensaje_2.find(':'));
			std::string vector_string2 = mensaje_2.substring(mensaje_2.find(':') + 1, mensaje_2.find(';') - mensaje_2.find(':') - 1);
			int i = std::stoi(vector_string1);
			int j = std::stoi(vector_string2);
			if (estadoGrid[i][j] == 2)
			{
				estadoGrid[i][j] = 0;
			}
			mensaje = "ACK_" + mensaje;
			networkmanager.send_data(&socket_client, mensaje, 5000);
		}


		sf::Event evento;
		while (window.pollEvent(evento))
		{

			switch (evento.type)
			{

			case sf::Event::Closed:
				window.close();
				break;
			case sf::Event::KeyPressed:
				break;
			case sf::Event::TextEntered:
				break;
			}
		}


		for (int i = 0; i < 11; i++) {
			window.draw(Hseparators[i]);
			window.draw(Vseparators[i]);
		}
		for (int j = 0; j < 10; j++) {
			for (int i = 0; i < 10; i++) {
				if (estadoGrid[i][j] == 0) {

				}
				else if (estadoGrid[i][j] == 1) {
					sf::CircleShape shape(4);
					shape.setFillColor(sf::Color(132, 198, 190));
					shape.setPosition(grid[i][j]);
					window.draw(shape);
				}
				else if (estadoGrid[i][j] == 2) {
					sf::CircleShape shape(4);
					shape.setFillColor(sf::Color(108, 70, 117));
					shape.setPosition(grid[i][j]);
					window.draw(shape);
				}
			}
		}
		window.display();
		window.clear();
	}
}

#pragma endregion