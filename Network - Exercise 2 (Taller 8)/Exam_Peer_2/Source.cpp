#include <SFML\Graphics.hpp>
#include <SFML\Network.hpp>
#include <string>
#include <iostream>
#include <vector>



//#include <Mouse.hpp>


class NetworkManager {
private:

public:
	void receive_data(std::vector<std::string>* aMensajes, sf::TcpSocket* socket_receive)
	{
		aMensajes->clear();
		char data[1000];
		size_t sizeRecv;
		sf::Socket::Status status_server;
		status_server = socket_receive->receive(data, 1000, sizeRecv);
		if (status_server == sf::Socket::NotReady)
		{
			//continue;
		}

		else if (status_server == sf::Socket::Done)
		{

			if (sizeRecv != 1000)
			{
				data[sizeRecv] = '\0';
			}

			sf::String mensaje = data;
			while (mensaje.find(';') != std::string::npos)
			{
				aMensajes->push_back(mensaje.substring(0, mensaje.find(';') + 1));
				mensaje = mensaje.substring(mensaje.find(';') + 1, mensaje.find('\0') - mensaje.find(';') - 1);
			}
		}
		else if (status_server == sf::Socket::Disconnected)
		{
			std::cout << "Peer 1 disconnected!" << std::endl;
			socket_receive->disconnect();
		}
	}


	void send_data(std::string data, sf::TcpSocket* socket_send)
	{
		sf::Socket::Status status_server;
		size_t bytesSent;
		status_server = socket_send->send(data.c_str(), data.length(), bytesSent);

		while (status_server == sf::Socket::Status::Partial)
		{
			data = data.substr(bytesSent, data.length());
			status_server = socket_send->send(data.c_str(), data.length() - bytesSent, bytesSent);
		}

		if (status_server == sf::Socket::Status::Disconnected)
		{
			socket_send->disconnect();
			std::cout << "Peer 1 disconnected!" << std::endl;
		}
	}
};


int main()
{

#pragma region connection

	NetworkManager network_manager;
	sf::Socket::Status status_peer_2;
	sf::TcpSocket socket_peer_2;

	do {
		status_peer_2 = socket_peer_2.connect("127.0.0.1", 5000, sf::seconds(0.5f));
		if (status_peer_2 != sf::Socket::Done)
		{
			std::cout << "Fallo el intento de conexion: " << status_peer_2 << " en el puerto: 5000" << std::endl;
		}
		else if (status_peer_2 == sf::Socket::Done)
		{
			std::cout << "Se conecto en 5000." << std::endl;
			break;
		}
	} while (status_peer_2 != sf::Socket::Done);


	std::cout << "Se conecto al servidor." << std::endl;

	socket_peer_2.setBlocking(false);
#pragma endregion





#pragma region chat

	sf::Vector2i screenDimensions(800, 600);

	sf::RenderWindow window;
	window.create(sf::VideoMode(screenDimensions.x, screenDimensions.y), "Chat");

	sf::Font font;
	if (!font.loadFromFile("courbd.ttf"))
	{
		std::cout << "Can't load the font file" << std::endl;
	}

	sf::String mensaje;

	sf::String header1 = " (1) Play Sound";
	sf::String header2 = "(2) Stop Sound";
	sf::String header3 = "(3 num_monsters) View Monsters" ;
	sf::String header4 = "(4) Hide Monsters";
	
	sf::Text headerText1(header1, font, 14);
	headerText1.setFillColor(sf::Color(0, 160, 0));
	headerText1.setStyle(sf::Text::Bold);
	headerText1.setPosition(0, 160);

	sf::Text headerText2(header2, font, 14);
	headerText2.setFillColor(sf::Color(0, 160, 0));
	headerText2.setStyle(sf::Text::Bold);
	headerText2.setPosition(0, 220);

	sf::Text headerText3(header3, font, 14);
	headerText3.setFillColor(sf::Color(0, 160, 0));
	headerText3.setStyle(sf::Text::Bold);
	headerText3.setPosition(0, 380);

	sf::Text headerText4(header4, font, 14);
	headerText4.setFillColor(sf::Color(0, 160, 0));
	headerText4.setStyle(sf::Text::Bold);
	headerText4.setPosition(0, 440);


	sf::Text textP1(mensaje, font, 14);
	textP1.setFillColor(sf::Color(0, 160, 0));
	textP1.setStyle(sf::Text::Bold);
	textP1.setPosition(0, 560);

	sf::RectangleShape canvas(sf::Vector2f(800, 800));
	canvas.setFillColor(sf::Color(00, 200, 200, 255));
	canvas.setPosition(0, 100);


	while (true)
	{
		sf::Event evento;
		while (window.pollEvent(evento))
		{

			switch (evento.type)
			{

			case sf::Event::Closed:
				socket_peer_2.disconnect();
				window.close();
				break;
			case sf::Event::KeyPressed:
				if (evento.key.code == sf::Keyboard::Escape)
				{
					socket_peer_2.disconnect();
					window.close();
				}					
				else if (evento.key.code == sf::Keyboard::Return)
				{
					std::string auxiliar_mensaje = mensaje;
					switch (auxiliar_mensaje.at(0))
					{
					case '1':
						auxiliar_mensaje += "_;";
						network_manager.send_data(auxiliar_mensaje, &socket_peer_2);
						break;

					case '2':
						auxiliar_mensaje += "_;";
						network_manager.send_data(auxiliar_mensaje, &socket_peer_2);
						break;

					case '3':
						auxiliar_mensaje = auxiliar_mensaje.substr(auxiliar_mensaje.find(" ") +1);
						auxiliar_mensaje = "3_" + auxiliar_mensaje + ";";
						network_manager.send_data(auxiliar_mensaje, &socket_peer_2);
						break;

					case '4':
						auxiliar_mensaje += "_;";
						network_manager.send_data(auxiliar_mensaje, &socket_peer_2);
						break;

					deafult:
						break;
					}
					mensaje = "";
				}

				break;
			case sf::Event::TextEntered:
					if (evento.text.unicode >= 32 && evento.text.unicode <= 126)
					{
						mensaje += (char)evento.text.unicode;
					}
					else if (evento.text.unicode == 8 && mensaje.getSize() > 0)
					{
						mensaje.erase(mensaje.getSize() - 1, mensaje.getSize());
					}
				break;
			}
		}

		window.draw(canvas);

		headerText1.setString(header1);
		headerText2.setString(header2);
		headerText3.setString(header3);
		headerText4.setString(header4);
		textP1.setString(mensaje);

		window.draw(textP1);
		window.draw(headerText1);
		window.draw(headerText2);
		window.draw(headerText3);
		window.draw(headerText4);



		window.display();
		window.clear();
	}
}

#pragma endregion
