#include <SFML\Graphics.hpp>
#include <SFML\Network.hpp>
#include <SFML\Audio.hpp>
#include <string>
#include <iostream>
#include <vector>



//#include <Mouse.hpp>
bool should_listen = false;

class NetworkManager {
private:

public:
	void receive_data(std::vector<std::string>* aMensajes, sf::TcpSocket* socket_receive)
	{
		aMensajes->clear();
		char data[1000];
		size_t sizeRecv;
		sf::Socket::Status status_server;
		status_server = socket_receive->receive(data, 1000, sizeRecv);
		if (status_server == sf::Socket::NotReady)
		{
			//continue;
		}

		else if (status_server == sf::Socket::Done)
		{

			if (sizeRecv != 1000)
			{
				data[sizeRecv] = '\0';
			}

			sf::String mensaje = data;
			while (mensaje.find(';') != std::string::npos)
			{
				aMensajes->push_back(mensaje.substring(0, mensaje.find(';') + 1));
				mensaje = mensaje.substring(mensaje.find(';') + 1, mensaje.find('\0') - mensaje.find(';') - 1);
			}
		}
		else if (status_server == sf::Socket::Disconnected)
		{
			std::cout << "Peer 2 disconnected! Waiting for a new connection..." << std::endl;
			socket_receive->disconnect();
			should_listen = true;
		}
	}


	void send_data(std::string data, sf::TcpSocket* socket_send)
	{
		sf::Socket::Status status_server;
		size_t bytesSent;
		status_server = socket_send->send(data.c_str(), data.length(), bytesSent);

		while (status_server == sf::Socket::Status::Partial)
		{
			data = data.substr(bytesSent, data.length());
			status_server = socket_send->send(data.c_str(), data.length() - bytesSent, bytesSent);
		}

		if (status_server == sf::Socket::Status::Disconnected)
		{
			std::cout << "Peer 2 disconnected! Waiting for a new connection..." << std::endl;
			socket_send->disconnect();
			should_listen = true;
		}
	}
};




int main()
{

#pragma region connection


	NetworkManager network_manager;
	sf::TcpListener listener;
	sf::Socket::Status status_peer_1;
	sf::TcpSocket socket_peer_1;
	socket_peer_1.setBlocking(false);
	std::vector<std::string> aMensajes;

	status_peer_1 = listener.listen(5000);

	status_peer_1 = listener.accept(socket_peer_1);
	if (status_peer_1 != sf::Socket::Status::Done)
	{
		std::cout << "Error al escuchar por el puerto 5000." << std::endl;
	}
	std::cout << "Se conecto al servidor." << std::endl;

#pragma endregion





#pragma region chat


	sf::Vector2i screenDimensions(800, 600);

	sf::RenderWindow window;
	window.create(sf::VideoMode(screenDimensions.x, screenDimensions.y), "Chat");

	sf::RectangleShape canvas(sf::Vector2f(800, 800));
	canvas.setFillColor(sf::Color(00, 00, 00, 255));
	canvas.setPosition(0, 100);

	sf::Font font;
	if (!font.loadFromFile("courbd.ttf"))
	{
		std::cout << "Can't load the font file" << std::endl;
	}

	/*sf::SoundBuffer soundBuffer;
	if (!soundBuffer.loadFromFile("terror.wav"))
	{
		std::cout << "Error al cargar sonido terror.wav." << std::endl;
	}
	sf::Sound sound;
	sound.setBuffer(soundBuffer);
	sound.setLoop(true);


	sf::Texture imageSource;
	if (imageSource.loadFromFile("monster.png"))
	{
		std::cout << "Error al cargar imagen" << std::endl;
	}

	std::vector<sf::Sprite> monsterImages;*/

	std::vector <sf::RectangleShape> squareImages;

	while (true)
	{
		if (should_listen == true)
		{
			status_peer_1 = listener.accept(socket_peer_1);
			if (status_peer_1 != sf::Socket::Status::Done)
			{
				std::cout << "Error al escuchar por el puerto 5000." << std::endl;
			}
			std::cout << "Se conecto al servidor." << std::endl;
			should_listen = false;
		}
		// We receive
		network_manager.receive_data(&aMensajes, &socket_peer_1);
		for (int i = 0; i < aMensajes.size(); i++)
		{
			sf::String mensaje = aMensajes[i];
			sf::String auxiliar_mensaje = mensaje.substring(0, mensaje.find('_'));
			if (auxiliar_mensaje == "1")
			{
				//sound.play();
				std::cout << "Sound would play here" << std::endl;
			}
			else if (auxiliar_mensaje == "2")
			{
				//sound.stop();
				std::cout << "Sound would stop here" << std::endl;
			}
			else if (auxiliar_mensaje == "3")
			{
				mensaje = mensaje.substring(mensaje.find('_') + 1, mensaje.find(';') - mensaje.find('_') - 1);
				std::string auxiliar = mensaje;
				int monsters = std::stoi(auxiliar);
				std::cout << "number of monsters " + auxiliar << std::endl;
				//monsterImages.clear();
				int seed = static_cast<int>(time(0));
				for (int i = 0; i < monsters; i++)
				{
					sf::RectangleShape temp_rect (sf::Vector2f(100, 100));
					//sf::Sprite imageSprite;
					//imageSprite.setTexture(imageSource, true);				
					srand(seed);
					int x = rand() % 799;
					int y = rand() % 799;
					std::cout << "Random Position " + std::to_string(x) + " " + std::to_string(y) << std::endl;
					//imageSprite.setPosition(x, y);
					//monsterImages.push_back(imageSprite);
					temp_rect.setFillColor(sf::Color(200, 100, 00, 255));
					temp_rect.setPosition(x, y);
					squareImages.push_back(temp_rect);
				}
			}
			else if (auxiliar_mensaje == "4")
			{
				std::cout << "Monsters would clear here" << std::endl;
				//monsterImages.clear();
				squareImages.clear();
			}
			
		}
		sf::Event evento;
		while (window.pollEvent(evento))
		{

			switch (evento.type)
			{

			case sf::Event::Closed:
				window.close();
				break;
			
			}
		}

		/*for (int i = 0; i < monsterImages.size(); i++)
		{
			window.draw(monsterImages[i]);
		}*/

	

		//window.draw(canvas);
		for (int i = 0; i < squareImages.size(); i++)
		{
			window.draw(squareImages[i]);
		}
		window.display();
		window.clear();
	}
}

#pragma endregion
