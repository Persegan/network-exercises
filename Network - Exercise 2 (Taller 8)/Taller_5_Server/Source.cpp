#include <SFML\Graphics.hpp>
#include <SFML\Network.hpp>
#include <iostream>
#include <vector>

using namespace std;

bool shouldclose = false;

class timer {
private:
	unsigned long begTime;
public:
	void start() {
		begTime = clock();
	}

	unsigned long elapsedTime() {
		return ((unsigned long)clock() - begTime) / CLOCKS_PER_SEC;
	}

	bool isTimeout(unsigned long seconds) {
		return seconds >= elapsedTime();
	}
};


//Network manager class with UDP sockets. 
//Receive returns a std::string containing the contents of receiving using a certain socket from a certain IP and port.
//Send sends a certain string using a certain udp socket to a specific IP and port.
class NetworkManager {
private:

public:
	void receive_data(sf::UdpSocket* socket_receive, sf::IpAddress* rIp, unsigned short* port, sf::String* message, std::size_t* size)
	{
		char buffer[1000];
		sf::Socket::Status status_server = socket_receive->receive(buffer, sizeof(buffer), *size, *rIp, *port);

		if (status_server == sf::Socket::NotReady)
		{
			//continue;
		}

		else if (status_server == sf::Socket::Done)
		{
			*message = std::string(buffer);
		}
		else if (status_server == sf::Socket::Disconnected)
		{
			std::cout << "There was a disconnection." << std::endl;
		}
	}

	void send_data(sf::UdpSocket* socket_send, std::string message, unsigned short port)
	{
		sf::IpAddress sIp = sf::IpAddress::getLocalAddress();
		char buffer[1000];
		strncpy_s(buffer, message.c_str(), sizeof(buffer));
		buffer[sizeof(buffer) - 1] = 0;
		if (socket_send->send(buffer, sizeof(buffer), sIp, port) != sf::Socket::Done)
		{
			std::cout << "There was an error while sending" << std::endl;
		}
	}
};

class Player
{
private:

public:
	int id;
	int position_x;
	int position_y;
	sf::IpAddress ip;
	unsigned short port;
	std::vector <sf::String> messages_no_ack;
};



int main()
{
	sf::UdpSocket socket_server;
	std::vector<Player> player_list;
	NetworkManager networkmanager;
	int nextplayerid = 0;

	sf::Socket::Status status_server = socket_server.bind(5000);

	if (status_server != sf::Socket::Done)
	{
		std::cout << "Error when binding socket to port 5000";
	}

	socket_server.setBlocking(false);

	timer no_ack_timer;
	no_ack_timer.start();
	timer ping_timer;
	ping_timer.start();

	while (true)
	{
		//Code to close the server when we need to
		if (shouldclose == true)
		{
			break;
		}

		//We receive
		sf::String mensaje;
		sf::IpAddress rIp;
		unsigned short port;
		std::size_t size;

		networkmanager.receive_data(&socket_server, &rIp, &port, &mensaje, &size);

		sf::String auxiliar_mensaje = mensaje.substring(0, mensaje.find('_'));

		if (auxiliar_mensaje == "ACK")
		{
			sf::String auxiliar_mensaje = mensaje.substring(mensaje.find('_')+1);
			//auxiliar_mensaje = auxiliar_mensaje.substring(auxiliar_mensaje.find('_') + 1, auxiliar_mensaje.find(';') - mensaje.find('_') - 1);
			for (int i = 0; i < player_list.size(); i++)
			{
				if (player_list[i].port == port)
				{
					for (int j = 0; j < player_list[i].messages_no_ack.size(); j++)
					{
						if (auxiliar_mensaje == player_list[i].messages_no_ack[j])
						{
							player_list[i].messages_no_ack.erase(player_list[i].messages_no_ack.begin() + j);
							break;
						}						
					}
					break;
				}
			}
		}


		else if (auxiliar_mensaje == "HELLO")
		{
			bool old_player = false;
			for (int i = 0; i < player_list.size(); i++)
			{
				if (port == player_list[i].port)
				{
					std::string auxiliar_message = "WELCOME_" + std::to_string(player_list[i].id) + ";";
					networkmanager.send_data(&socket_server, auxiliar_message, port);
					old_player = true;
					break;
				}
			}
			if (old_player == false)
			{
				Player auxiliar_player;
				nextplayerid++;
				auxiliar_player.id = nextplayerid;
				auxiliar_player.ip = rIp;
				auxiliar_player.port = port; 
				mensaje = mensaje.substring(mensaje.find('_') + 1, mensaje.find(';') - mensaje.find('_') - 1);
				std::string vector_string1 = mensaje.substring(0, mensaje.find(':'));
				std::string vector_string2 = mensaje.substring(mensaje.find(':') + 1, mensaje.find(';') - mensaje.find(':') - 1);
				auxiliar_player.position_x = std::stoi(vector_string1);
				auxiliar_player.position_y = std::stoi(vector_string2);

				std::string auxiliar_message = "WELCOME_" + std::to_string(nextplayerid) + ";";
				networkmanager.send_data(&socket_server, auxiliar_message, port);
				player_list.push_back(auxiliar_player);

				for (int i = 0; i < player_list.size()-1; i++)
				{
						std::string mensaje_2 = "NEWPLAYER_" + std::to_string(player_list[i].position_x) + ":" + std::to_string(player_list[i].position_y);
						networkmanager.send_data(&socket_server, auxiliar_message, port);
						player_list[player_list.size()-1].messages_no_ack.push_back(mensaje_2);

				}
				for (int i = 0; i < player_list.size(); i++)
				{
					std::string auxiliar_message = "NEWPLAYER_" + vector_string1 + ":" + vector_string2 +  ";";
					networkmanager.send_data(&socket_server, auxiliar_message, player_list[i].port);
					player_list[i].messages_no_ack.push_back(auxiliar_message);
				}
				
			}
		}



		//We send all no ack messages
		if (no_ack_timer.elapsedTime() > 1.0f)
		{
			no_ack_timer.start();
			for (int i = 0; i < player_list.size(); i++)
			{
				for (int j = 0; j < player_list[i].messages_no_ack.size(); j++)
				{
					std::string auxiliar_message = player_list[i].messages_no_ack[j];
					networkmanager.send_data(&socket_server, auxiliar_message, player_list[i].port);
				}
			}
		}


		//We send all ping messages and disconnect people who haven't answered
		if (ping_timer.elapsedTime() > 2.0f)
		{
			for (int i = 0; i < player_list.size(); i++)
			{
				for (int j = 0; j < player_list[i].messages_no_ack.size(); j++)
				{
					if (player_list[i].messages_no_ack[j] == "PING_;")
					{
						std::string mensaje_2 = "REMOVEPLAYER_" + std::to_string(player_list[i].position_x) + ":" + std::to_string(player_list[i].position_y);
						player_list.erase(player_list.begin() + i);
						for (int k = 0; k < player_list.size(); k++)
						{							
							networkmanager.send_data(&socket_server, mensaje_2, player_list[k].port);
							player_list[k].messages_no_ack.push_back(mensaje_2);
						}
						i--;
					}
				}
			}

			for (int i = 0; i < player_list.size(); i++)
			{
				std::string auxiliar_message = "PING_;";
				networkmanager.send_data(&socket_server, auxiliar_message, player_list[i].port);
				player_list[i].messages_no_ack.push_back(auxiliar_message);
			}
			ping_timer.start();
		}		
	}
}