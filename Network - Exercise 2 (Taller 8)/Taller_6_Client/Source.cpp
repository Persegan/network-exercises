#include <SFML\Graphics.hpp>
#include <SFML\Network.hpp>
#include <string>
#include <iostream>
#include <vector>
#include "InputMemoryBitStream.h"
#include "InputMemoryStream.h"
#include "OutputMemoryBitStream.h"
#include "OutputMemoryStream.h"
#include <cstdint>


//#include <Mouse.hpp>

#define PACKET_BITS 3
#define PLAYER_ID_BITS 5
#define GRID_X_BITS 4
#define GRID_Y_BITS 4
#define PACKET_ID_BITS 7

bool shouldclose = false;

enum PacketType {
	PT_EMPTY = 0,
	HELLO,
	ACK,
	WELCOME,
	PING,
	NEWPLAYER,
	REMOVEPLAYER
};


class timer {
private:
	unsigned long begTime;
public:
	void start() {
		begTime = clock();
	}

	unsigned long elapsedTime() {
		return ((unsigned long)clock() - begTime) / CLOCKS_PER_SEC;
	}

	bool isTimeout(unsigned long seconds) {
		return seconds >= elapsedTime();
	}
};

//Network manager class with UDP sockets. 
//Receive returns a simbs containing the contents of receiving using a certain socket from a certain IP and port.
//Send sends a certain ombs using a certain udp socket to a specific IP and port.
class NetworkManager {
private:

public:
	InputMemoryBitStream receive_data(sf::UdpSocket* socket_receive, sf::IpAddress* rIp, unsigned short* port)
	{
		std::size_t size;
		char buffer[1000];
		sf::Socket::Status status_server = socket_receive->receive(buffer, 1000, size, *rIp, *port);


		if (status_server == sf::Socket::NotReady)
		{
			//continue;
		}

		else if (status_server == sf::Socket::Done)
		{
			InputMemoryBitStream imbs(buffer, size * 8);
			return imbs;
		}
		else if (status_server == sf::Socket::Disconnected)
		{
			std::cout << "There was a disconnection." << std::endl;
		}
	}

	void send_data(sf::UdpSocket* socket_send, OutputMemoryBitStream& _ombs, unsigned short port)
	{
		sf::IpAddress sIp = sf::IpAddress::getLocalAddress();
		if (socket_send->send(_ombs.GetBufferPtr(), _ombs.GetByteLength(), sIp, port) != sf::Socket::Done)
		{
			std::cout << "There was an error while sending" << std::endl;
		}
	}

	void send_data(sf::UdpSocket* socket_send, std::string message, unsigned short port)
	{
		sf::IpAddress sIp = sf::IpAddress::getLocalAddress();
		char buffer[1000];
		strncpy_s(buffer, message.c_str(), sizeof(buffer));
		buffer[sizeof(buffer) - 1] = 0;
		if (socket_send->send(buffer, sizeof(buffer), sIp, port) != sf::Socket::Done)
		{
			std::cout << "There was an error while sending" << std::endl;
		}
	}
};


int main()
{

#pragma region connection

	sf::Vector2f grid[10][10];
	for (int j = 0; j < 10; j++) {
		for (int i = 0; i < 10; i++) {
			grid[i][j] = sf::Vector2f(59.5f / 2 + 59.5f*i, 59.5f / 2 + 59.5f*j);

		}
	}
	int estadoGrid[10][10];
	//0 Nada
	//1 JugadorLocal
	//2 JugadorExterno
	for (int j = 0; j < 10; j++) {
		for (int i = 0; i < 10; i++) {
			estadoGrid[i][j] = 0;

		}
	}
	/*int seed = static_cast<int>(time(0));
	srand(seed);
	int initial_x = rand() % 10;
	int initial_y = rand() % 10;
	estadoGrid[initial_x][initial_y] = 1;*/
	sf::UdpSocket socket_client;
	NetworkManager networkmanager;
	int player_id = 0;
	timer timer1;
	socket_client.setBlocking(false);




#pragma endregion

	timer1.start();

	while (player_id == 0)
	{
		if (timer1.elapsedTime() > 0.4f)
		{


			OutputMemoryBitStream ombs;
			ombs.Write(PacketType::HELLO, PACKET_BITS);
			networkmanager.send_data(&socket_client, ombs, 5000);
			timer1.start();

		}

		sf::String mensaje_2;
		sf::IpAddress rIp;
		unsigned short port;
		std::size_t size;


		
		InputMemoryBitStream imbs = networkmanager.receive_data(&socket_client, &rIp, &port);

		PacketType pt = PacketType::PT_EMPTY;
		if (imbs.GetRemainingBitCount() > 0)
			imbs.Read(&pt, PACKET_BITS);

		if (pt == PacketType::WELCOME)
		{
			imbs.Read(&player_id, PLAYER_ID_BITS);
			int8_t i;
			int8_t j;
			imbs.Read(&i, GRID_X_BITS);
			imbs.Read(&j, GRID_Y_BITS);
			if (estadoGrid[i][j] == 0)
			{
				estadoGrid[i][j] = 1;
			}

		}
	}

#pragma region chat

	sf::Vector2i screenDimensions(800, 600);

	sf::RenderWindow window;
	window.create(sf::VideoMode(screenDimensions.x, screenDimensions.y), "Game");

	sf::Font font;
	if (!font.loadFromFile("courbd.ttf"))
	{
		std::cout << "Can't load the font file" << std::endl;
	}





	sf::RectangleShape temporal(sf::Vector2f(600, 5));
	sf::RectangleShape Hseparators[11];
	for (int i = 0; i < 11; i++)
	{
		Hseparators[i] = temporal;

		Hseparators[i].setFillColor(sf::Color(100, 200, 200, 255));
		Hseparators[i].setPosition(0, 59.5f * i);
	}
	sf::RectangleShape temporal2(sf::Vector2f(5, 800));
	sf::RectangleShape Vseparators[11];
	for (int i = 0; i < 11; i++)
	{
		Vseparators[i] = temporal2;

		Vseparators[i].setFillColor(sf::Color(100, 200, 200, 255));
		Vseparators[i].setPosition(59.5f * i, 0);
	}


	while (true)
	{
		if (shouldclose == true)
		{
			window.close();
		}


		//Send test

		// We receive
		sf::IpAddress rIp;
		unsigned short port;

		InputMemoryBitStream imbs = networkmanager.receive_data(&socket_client, &rIp, &port);

		PacketType pt = PacketType::PT_EMPTY;
		if (imbs.GetRemainingBitCount() > 0)
			imbs.Read(&pt, PACKET_BITS);

		if (pt == PacketType::PING)
		{
			int k = 0;
			imbs.Read(&k, PACKET_ID_BITS);
			OutputMemoryBitStream ombs;			
			ombs.Write(PacketType::ACK, PACKET_BITS);
			ombs.Write(k, PACKET_ID_BITS);			
			networkmanager.send_data(&socket_client, ombs, 5000);
		}

		else if (pt == PacketType::NEWPLAYER)
		{
			int8_t i;
			int8_t j;
			imbs.Read(&i, GRID_X_BITS);
			imbs.Read(&j, GRID_Y_BITS);
			if (estadoGrid[i][j] == 0)
			{
				estadoGrid[i][j] = 2;
			}

			int k;
			imbs.Read(&k, PACKET_ID_BITS);
			OutputMemoryBitStream ombs;
			ombs.Write(PacketType::ACK, PACKET_BITS);
			ombs.Write(k, PACKET_ID_BITS);

			networkmanager.send_data(&socket_client, ombs, 5000);
		}

		else if (pt == PacketType::REMOVEPLAYER)
		{
			int8_t i;
			int8_t j;
			imbs.Read(&i, GRID_X_BITS);
			imbs.Read(&j, GRID_Y_BITS);
			if (estadoGrid[i][j] == 2)
			{
				estadoGrid[i][j] = 0;
			}
			int k;
			imbs.Read(&k, PACKET_ID_BITS);
			OutputMemoryBitStream ombs;
			ombs.Write(PacketType::ACK, PACKET_BITS);
			ombs.Write(k, PACKET_ID_BITS);

			networkmanager.send_data(&socket_client, ombs, 5000);
		}


		sf::Event evento;
		while (window.pollEvent(evento))
		{

			switch (evento.type)
			{

			case sf::Event::Closed:
				window.close();
				break;
			case sf::Event::KeyPressed:
				break;
			case sf::Event::TextEntered:
				break;
			}
		}


		for (int i = 0; i < 11; i++) {
			window.draw(Hseparators[i]);
			window.draw(Vseparators[i]);
		}
		for (int j = 0; j < 10; j++) {
			for (int i = 0; i < 10; i++) {
				if (estadoGrid[i][j] == 0) {

				}
				else if (estadoGrid[i][j] == 1) {
					sf::CircleShape shape(4);
					shape.setFillColor(sf::Color(132, 198, 190));
					shape.setPosition(grid[i][j]);
					window.draw(shape);
				}
				else if (estadoGrid[i][j] == 2) {
					sf::CircleShape shape(4);
					shape.setFillColor(sf::Color(108, 70, 117));
					shape.setPosition(grid[i][j]);
					window.draw(shape);
				}
			}
		}
		window.display();
		window.clear();
	}
}

#pragma endregion