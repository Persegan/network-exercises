#include <SFML\Graphics.hpp>
#include <SFML\Network.hpp>
#include <string>
#include <iostream>
#include <vector>



//#include <Mouse.hpp>

#define MAX_MENSAJES 30

sf::Mutex mutex;
bool shouldclose = false;


class timer {
private:
	unsigned long begTime;
public:
	void start() {
		begTime = clock();
	}

	unsigned long elapsedTime() {
		return ((unsigned long)clock() - begTime) / CLOCKS_PER_SEC;
	}

	bool isTimeout(unsigned long seconds) {
		return seconds >= elapsedTime();
	}
};
class NetworkManager {
private:

public:
	void receive_data(std::vector<std::string>* aMensajes, sf::TcpSocket* socket_receive)
	{
		aMensajes->clear();
		char data[1000];
		size_t sizeRecv;
		sf::Socket::Status status_server;
		status_server = socket_receive->receive(data, 1000, sizeRecv);
		if (status_server == sf::Socket::NotReady)
		{
			//continue;
		}

		else if (status_server == sf::Socket::Done)
		{

			if (sizeRecv != 1000)
			{
				data[sizeRecv] = '\0';
			}

			sf::String mensaje = data;
			while (mensaje.find(';') != std::string::npos)
			{
				aMensajes->push_back(mensaje.substring(0, mensaje.find(';') + 1));
				mensaje = mensaje.substring(mensaje.find(';') + 1, mensaje.find('\0') - mensaje.find(';') - 1);
			}
			/*for (int i = 0; i < aMensajes.size(); i++)
			{
			mensaje = aMensajes[i];
			sf::String auxiliar_mensaje = mensaje.substring(0, mensaje.find('_') - 1);
			}*/
		}
		else if (status_server == sf::Socket::Disconnected)
		{
			std::cout << "Successfully disconnected." << std::endl;
			socket_receive->disconnect();
			shouldclose = true;
		}
	}


	void send_data(std::string data, sf::TcpSocket* socket_send)
	{
		sf::Socket::Status status_server;
		size_t bytesSent;
		status_server = socket_send->send(data.c_str(), data.length(), bytesSent);

		while (status_server == sf::Socket::Status::Partial)
		{
			data = data.substr(bytesSent, data.length());
			status_server = socket_send->send(data.c_str(), data.length() - bytesSent, bytesSent);
		}

		if (status_server == sf::Socket::Status::Disconnected)
		{
			socket_send->disconnect();
			std::cout << "Successfully disconnected." << std::endl;
			shouldclose = true;
		}
	}
};


int main()
{

#pragma region connection

	std::vector<sf::Vector2f> dibujo;
	std::vector<sf::Vector2f> dibujo_a_enviar;
	bool drawing_player = false;
	int round = 0;
	NetworkManager network_manager;
	sf::Socket::Status status_client;
	sf::TcpSocket sock_client;
	std::vector<std::string> aMensajesauxiliar;
	int player = 0;
	bool startgame = false;
	std::string player_name;
	std::cout << "Escoge tu nombre" << std::endl;
	std::cin >> player_name;
	do {
		status_client = sock_client.connect("127.0.0.1", 5000, sf::seconds(0.5f));
		if (status_client != sf::Socket::Done)
		{
			std::cout << "Fallo el intento de conexion: " << status_client << " en el puerto: 5000" << std::endl;
		}
		else if (status_client == sf::Socket::Done)
		{
			std::cout << "Se conecto en 5000." << std::endl;
			break;
		}
	} while (status_client != sf::Socket::Done);


	std::cout << "Se conecto al servidor." << std::endl;


	unsigned long seconds = 20;
	timer t;
	timer vector_t;
	t.start();

	sock_client.setBlocking(false);

	while (startgame == false)
	{

		network_manager.receive_data(&aMensajesauxiliar, &sock_client);
		for (int i = 0; i < aMensajesauxiliar.size(); i++)
		{
			if (aMensajesauxiliar[i] == "GAMESTART_player1;")
			{
				startgame = true;
				player = 1;
				player_name = "PLAYER1NAME_" + player_name + ";";
				break;
			}
			else if (aMensajesauxiliar[i] == "GAMESTART_player2;")
			{
				startgame = true;
				player = 2;
				player_name = "PLAYER2NAME_" + player_name + ";";
				break;
			}
		}
	}

	network_manager.send_data(player_name, &sock_client);
	sf::String mensajeNameP2 = "";
	if (player == 2)
	{
		mensajeNameP2 = player_name;
		mensajeNameP2 = mensajeNameP2.substring(mensajeNameP2.find('_') + 1, mensajeNameP2.find(';') - mensajeNameP2.find('_') - 1);
	}

	sf::String mensajeNameP1 = "";
	if (player == 1)
	{
		mensajeNameP1 = player_name;
		mensajeNameP1 = mensajeNameP1.substring(mensajeNameP1.find('_') + 1, mensajeNameP1.find(';') - mensajeNameP1.find('_') - 1);
	}

	while (mensajeNameP1 == "" || mensajeNameP2 == "")
	{
		network_manager.receive_data(&aMensajesauxiliar, &sock_client);
		for (int i = 0; i < aMensajesauxiliar.size(); i++)
		{
			sf::String mensaje = aMensajesauxiliar[i];
			sf::String auxiliar_mensaje = mensaje.substring(0, mensaje.find('_'));
			if (auxiliar_mensaje == "PLAYER1NAME")
			{
				mensaje = mensaje.substring(mensaje.find('_') + 1, mensaje.find(';') - mensaje.find('_') - 1);
				mensajeNameP1 = mensaje;
			}
			if (auxiliar_mensaje == "PLAYER2NAME")
			{
				mensaje = mensaje.substring(mensaje.find('_') + 1, mensaje.find(';') - mensaje.find('_') - 1);
				mensajeNameP2 = mensaje;
			}
		}
	}



	/*char data[100];
	size_t sizeRecv;
	status_client = sock_client.receive(data, 100, sizeRecv);
	std::cout << "Status: " << status_client << std::endl;
	if (status_client != sf::Socket::Done)
	{
	std::cout << "Error al recibir." << std::endl;
	}
	data[sizeRecv] = '\0';
	std::cout << "Recibo: " << data << std::endl;*/

#pragma endregion





#pragma region chat

	std::vector<std::string> aMensajes;
	std::vector<std::string> aMensajes2;

	sf::Vector2i screenDimensions(800, 600);

	sf::RenderWindow window;
	window.create(sf::VideoMode(screenDimensions.x, screenDimensions.y), "Chat");

	sf::Font font;
	if (!font.loadFromFile("courbd.ttf"))
	{
		std::cout << "Can't load the font file" << std::endl;
	}

	sf::String mensaje = " >";

	sf::String mensajep1;

	sf::String mensajep2;

	sf::String mensajeToWrite = "Guess the word.";

	sf::String mensajeTimer = " Timer";

	sf::String mensajeScoreP1 = "0";


	sf::String mensajeScoreP2 = "0";

	sf::Text chattingTextP1(mensaje, font, 14);
	chattingTextP1.setFillColor(sf::Color(0, 160, 0));
	chattingTextP1.setStyle(sf::Text::Bold);

	sf::Text chattingTextP2(mensaje, font, 14);
	chattingTextP2.setFillColor(sf::Color(0, 160, 0));
	chattingTextP2.setStyle(sf::Text::Bold);


	sf::Text textP1(mensaje, font, 14);
	textP1.setFillColor(sf::Color(0, 160, 0));
	textP1.setStyle(sf::Text::Bold);
	textP1.setPosition(0, 560);

	sf::Text textP2(mensaje, font, 14);
	textP2.setFillColor(sf::Color(0, 160, 0));
	textP2.setStyle(sf::Text::Bold);
	textP2.setPosition(450, 560);

	sf::Text textToWrite(mensajeToWrite, font, 14);
	textToWrite.setFillColor(sf::Color(255, 255, 0));
	textToWrite.setStyle(sf::Text::Bold);
	textToWrite.setPosition(20, 20);

	sf::Text timer(mensajeTimer, font, 14);
	timer.setFillColor(sf::Color(255, 0, 255));
	timer.setStyle(sf::Text::Bold);
	timer.setPosition(500, 20);

	sf::Text nameP1(mensajeNameP1, font, 14);
	nameP1.setFillColor(sf::Color(255, 0, 255));
	nameP1.setStyle(sf::Text::Bold);
	nameP1.setPosition(20, 50);

	sf::Text scoreP1(mensajeScoreP1, font, 14);
	scoreP1.setFillColor(sf::Color(255, 0, 255));
	scoreP1.setStyle(sf::Text::Bold);
	scoreP1.setPosition(300, 50);

	sf::Text nameP2(mensajeNameP2, font, 14);
	nameP2.setFillColor(sf::Color(255, 0, 255));
	nameP2.setStyle(sf::Text::Bold);
	nameP2.setPosition(470, 50);

	sf::Text scoreP2(mensajeScoreP2, font, 14);
	scoreP2.setFillColor(sf::Color(255, 0, 255));
	scoreP2.setStyle(sf::Text::Bold);
	scoreP2.setPosition(700, 50);

	sf::RectangleShape canvas(sf::Vector2f(800, 800));
	canvas.setFillColor(sf::Color(00, 200, 200, 255));
	canvas.setPosition(0, 100);

	sf::RectangleShape separator1(sf::Vector2f(800, 5));
	sf::RectangleShape separator(sf::Vector2f(800, 5));
	sf::RectangleShape separator2(sf::Vector2f(800, 5));
	sf::RectangleShape separator3(sf::Vector2f(5, 800));
	sf::RectangleShape separator4(sf::Vector2f(5, 100));
	separator.setFillColor(sf::Color(200, 200, 200, 255));
	separator.setPosition(0, 550);
	separator1.setFillColor(sf::Color(200, 200, 200, 255));
	separator1.setPosition(0, 100);
	separator2.setFillColor(sf::Color(200, 200, 200, 255));
	separator2.setPosition(0, 200);
	separator3.setFillColor(sf::Color(200, 200, 200, 255));
	separator3.setPosition(410, 100);
	separator4.setFillColor(sf::Color(200, 200, 200, 255));
	separator4.setPosition(420, 0);


	while (true)
	{
			if (shouldclose == true)
			{
				window.close();
			}
			// We receive
			network_manager.receive_data(&aMensajesauxiliar, &sock_client);
			for (int i = 0; i < aMensajesauxiliar.size(); i++)
			{
				sf::String mensaje = aMensajesauxiliar[i];
				sf::String auxiliar_mensaje = mensaje.substring(0, mensaje.find('_'));
				if (auxiliar_mensaje == "TIMERSTART")
				{
					mensaje = mensaje.substring(mensaje.find('_') + 1, mensaje.find(';') - mensaje.find('_') - 1);
					std::string potato = mensaje;
					seconds = std::stoi(potato);
					t.start();
					dibujo.clear();
					dibujo_a_enviar.clear();
				}
				else if (auxiliar_mensaje == "WORDTOTYPE")
				{
					mensaje = mensaje.substring(mensaje.find('_') + 1, mensaje.find(';') - mensaje.find('_') - 1);
					mensajeToWrite = mensaje;
				}
				else if (auxiliar_mensaje == "GAMEEND")
				{
					mensaje = mensaje.substring(mensaje.find('_') + 1, mensaje.find(';') - mensaje.find('_') - 1);
					if (mensaje == "player1")
					{
						std::string potato = mensajeNameP1;
						std::cout << potato + " wins!" << std::endl;
					}
					else if (mensaje == "player2")
					{
						std::string potato = mensajeNameP2;
						std::cout << potato + " wins!" << std::endl;
					}
					else if (mensaje == "draw")
					{
						std::cout << "It's a draw!" << std::endl;
					}

				}
				else if (auxiliar_mensaje == "PLAYER1ADDCHAR")
				{
					mensaje = mensaje.substring(mensaje.find('_') + 1, mensaje.find(';') - mensaje.find('_') - 1);
					mensajep1 += mensaje;
				}
				else if (auxiliar_mensaje == "PLAYER2ADDCHAR")
				{
					mensaje = mensaje.substring(mensaje.find('_') + 1, mensaje.find(';') - mensaje.find('_') - 1);
					mensajep2 += mensaje;
				}
				else if (auxiliar_mensaje == "PLAYER1ADDTOLOG")
				{
					mensaje = mensaje.substring(mensaje.find('_') + 1, mensaje.find(';') - mensaje.find('_') - 1);
					if (mensaje == "correct")
					{
						aMensajes.push_back(mensajep1 + " -- CORRECT");
						if (aMensajes.size() > 5)
						{
							aMensajes.erase(aMensajes.begin(), aMensajes.begin() + 1);
						}
					}
					else
					{
						aMensajes.push_back(mensajep1 + " -- INCORRECT");
						if (aMensajes.size() > 5)
						{
							aMensajes.erase(aMensajes.begin(), aMensajes.begin() + 1);
						}
					}
					mensajep1.clear();
				}
				else if (auxiliar_mensaje == "PLAYER2ADDTOLOG")
				{
					mensaje = mensaje.substring(mensaje.find('_') + 1, mensaje.find(';') - mensaje.find('_') - 1);
					if (mensaje == "correct")
					{
						aMensajes2.push_back(mensajep2 + " -- CORRECT");
						if (aMensajes2.size() > 25)
						{
							aMensajes2.erase(aMensajes2.begin(), aMensajes2.begin() + 1);
						}
					}
					else
					{
						aMensajes2.push_back(mensajep2 + " -- INCORRECT");
						if (aMensajes2.size() > 25)
						{
							aMensajes2.erase(aMensajes.begin(), aMensajes2.begin() + 1);
						}
					}
					mensajep2.clear();
				}
				else if (auxiliar_mensaje == "SCORE1")
				{
					mensaje = mensaje.substring(mensaje.find('_') + 1, mensaje.find(';') - mensaje.find('_') - 1);
					mensajeScoreP1 = mensaje;
				}
				else if (auxiliar_mensaje == "SCORE2")
				{
					mensaje = mensaje.substring(mensaje.find('_') + 1, mensaje.find(';') - mensaje.find('_') - 1);
					mensajeScoreP2 = mensaje;
				}
				else if (auxiliar_mensaje == "PLAYER1REMOVECHAR")
				{
					if (mensajep1.getSize() > 0)
					{
						mensajep1.erase(mensajep1.getSize() - 1, mensajep1.getSize());
					}

				}
				else if (auxiliar_mensaje == "PLAYER2REMOVECHAR")
				{
					if (mensajep2.getSize() > 0)
					{
						mensajep2.erase(mensajep2.getSize() - 1, mensajep2.getSize());
					}
				}
				else if (auxiliar_mensaje == "DRAWINGPLAYER")
				{
					mensaje = mensaje.substring(mensaje.find('_') + 1, mensaje.find(';') - mensaje.find('_') - 1);
					if (mensaje == "true")
					{
						drawing_player = true;
						vector_t.start();
					}
					else if (mensaje == "false")
					{
						drawing_player = false;
						mensajeToWrite = "Guess the word.";
					}
					mensajep1 = "";
					mensajep2 = "";
				}
				else if (auxiliar_mensaje == "ADDVECTOR")
				{
					mensaje = mensaje.substring(mensaje.find('_') + 1, mensaje.find(';') - mensaje.find('_') - 1);
					std::string vector_string1 = mensaje.substring(0, mensaje.find(':'));
					std::string vector_string2 = mensaje.substring(mensaje.find(':') + 1, mensaje.find(';') - mensaje.find(':') - 1);
					sf::Vector2f auxiliar_vector = sf::Vector2f(std::stof(vector_string1), std::stof(vector_string2));
					dibujo.push_back(auxiliar_vector);
				}
			}

			/*aMensajes.push_back(mensaje);
			if (aMensajes.size() > 25)
			{
			aMensajes.erase(aMensajes.begin(), aMensajes.begin() + 1);
			}*/



			sf::Event evento;
			while (window.pollEvent(evento))
			{
				
				switch (evento.type)
				{

				case sf::Event::Closed:
					sock_client.disconnect();
					window.close();
					break;
				case sf::Event::KeyPressed:
					if (drawing_player == false)
					{
						if (evento.key.code == sf::Keyboard::Escape)
							window.close();
						else if (evento.key.code == sf::Keyboard::Return)
						{
							//We Send
							std::string auxiliar_mensaje;
							if (player == 1)
							{
								auxiliar_mensaje = "PLAYER1SEND_" + mensajep1 + ";";
							}
							else if (player == 2)
							{
								auxiliar_mensaje = "PLAYER2SEND_" + mensajep2 + ";";
							}
							network_manager.send_data(auxiliar_mensaje, &sock_client);
							//mensaje = ">";
							//mutex.unlock();
						}
					}

					break;
				case sf::Event::TextEntered:
					if (drawing_player == false)
					{
						if (evento.text.unicode >= 32 && evento.text.unicode <= 126)
						{
							std::string auxiliar_mensaje = "";
							if (player == 1)
							{
								char cst;
								cst = (char)evento.text.unicode;
								std::string str(1, cst);
								auxiliar_mensaje = "PLAYER1ADDCHAR_" + str + ";";
							}
							else if (player == 2)
							{
								char cst;
								cst = (char)evento.text.unicode;
								std::string str(1, cst);
								auxiliar_mensaje = "PLAYER2ADDCHAR_" + str + ";";
							}
							network_manager.send_data(auxiliar_mensaje, &sock_client);
							//mensaje += (char)evento.text.unicode;
						}
						else if (evento.text.unicode == 8 && mensaje.getSize() > 0)
						{
							std::string auxiliar_mensaje;
							if (player == 1)
							{
								auxiliar_mensaje = "PLAYER1REMOVECHAR_;";
							}
							else if (player == 2)
							{
								auxiliar_mensaje = "PLAYER2REMOVECHAR_;";
							}
							network_manager.send_data(auxiliar_mensaje, &sock_client);
							//mensaje.erase(mensaje.getSize() - 1, mensaje.getSize());
						}
					}
					break;
				}
			}
				
			window.draw(canvas);
			//window.draw(separator);
			window.draw(separator1);
			//window.draw(separator2);
			//window.draw(separator3);
			window.draw(separator4);


			

			if (sf::Mouse::isButtonPressed(sf::Mouse::Left)) {
				if (drawing_player == true)
				{
					if (static_cast<sf::Vector2f>(sf::Mouse::getPosition(window)).y > 100) {
						dibujo.push_back(static_cast<sf::Vector2f>(sf::Mouse::getPosition(window)));
						dibujo_a_enviar.push_back(static_cast<sf::Vector2f>(sf::Mouse::getPosition(window)));
					}
				}				
			}
			if (drawing_player == true)
			{
				if (vector_t.elapsedTime() == 2)
				{
					if (dibujo_a_enviar.size() > 0)
					{

						for (int i = 0; i < dibujo_a_enviar.size(); i++)
						{
							std::string auxiliar_mensaje = "";
							int auxiliarintx = (int)dibujo_a_enviar[i].x;
							int auxiliarinty = (int)dibujo_a_enviar[i].y;
							//mensaje = "ADDVECTOR_" + std::to_string(dibujo_a_enviar[i].x) + ":" + std::to_string(dibujo_a_enviar[i].y);
							auxiliar_mensaje = "ADDVECTOR_" + std::to_string(auxiliarintx) + ":" + std::to_string(auxiliarinty) + ";";
							network_manager.send_data(auxiliar_mensaje, &sock_client);
						}
						dibujo_a_enviar.clear();
					}
					vector_t.start();
				}
			}	


			for (int i = 0; i < dibujo.size(); i++) {
				sf::CircleShape shape(4);
				shape.setPosition(dibujo[i]);
				shape.setFillColor(sf::Color(100, 250, 50));
				window.draw(shape);
			}
			for (size_t i = 0; i < aMensajes.size(); i++)
			{
				std::string chatting = aMensajes[i];
				chattingTextP1.setPosition(sf::Vector2f(0, 200 + 20 * i));
				chattingTextP1.setString(chatting);
				window.draw(chattingTextP1);
			}

			for (size_t i = 0; i < aMensajes2.size(); i++)
			{
				std::string chatting2 = aMensajes2[i];
				chattingTextP2.setPosition(sf::Vector2f(450, 200 + 20 * i));
				chattingTextP2.setString(chatting2);
				window.draw(chattingTextP2);
			}
			std::string mensaje_ = mensaje + "_";
			textP1.setString(mensajep1);
			textP2.setString(mensajep2);
			textToWrite.setString(mensajeToWrite);
			timer.setString(std::to_string(seconds - t.elapsedTime()));
			nameP1.setString(mensajeNameP1);
			scoreP1.setString(mensajeScoreP1);
			nameP2.setString(mensajeNameP2);
			scoreP2.setString(mensajeScoreP2);

			window.draw(textP1);
			window.draw(textP2);
			window.draw(textToWrite);
			window.draw(timer);
			window.draw(nameP1);
			window.draw(scoreP1);
			window.draw(nameP2);
			window.draw(scoreP2);



			window.display();
			window.clear();
		}
} 

#pragma endregion
