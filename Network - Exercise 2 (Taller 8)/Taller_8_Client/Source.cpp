#include <SFML\Graphics.hpp>
#include <SFML\Network.hpp>
#include <string>
#include <iostream>
#include <vector>
#include "InputMemoryBitStream.h"
#include "InputMemoryStream.h"
#include "OutputMemoryBitStream.h"
#include "OutputMemoryStream.h"
#include <cstdint>


//#include <Mouse.hpp>

#define PACKET_BITS 3
#define PLAYER_ID_BITS 3
#define GRID_X_BITS 10
#define GRID_Y_BITS 10
#define MAX_PACKETS_VECTOR_SIZE 32
#define PACKET_ID_BITS 5
#define WINDOWS_X 800
#define WINDOWS_Y 600
#define DELTA_X_BITS 6
#define DELTA_Y_BITS 6
bool shouldclose = false;

enum PacketType {
	PT_EMPTY = 0,
	HELLO,
	ACK,
	WELCOME,
	PING,
	NEWPLAYER,
	REMOVEPLAYER,
	MOVEPLAYER
};


class timer {
private:
	unsigned long begTime;
public:
	void start() {
		begTime = clock();
	}

	unsigned long elapsedTime() {
		return ((unsigned long)clock() - begTime) / CLOCKS_PER_SEC;
	}

	bool isTimeout(unsigned long seconds) {
		return seconds >= elapsedTime();
	}
};

//Network manager class with UDP sockets. 
//Receive returns a simbs containing the contents of receiving using a certain socket from a certain IP and port.
//Send sends a certain ombs using a certain udp socket to a specific IP and port.
class NetworkManager {
private:

public:
	InputMemoryBitStream receive_data(sf::UdpSocket* socket_receive, sf::IpAddress* rIp, unsigned short* port)
	{
		std::size_t size;
		char buffer[1000];
		sf::Socket::Status status_server = socket_receive->receive(buffer, 1000, size, *rIp, *port);


		if (status_server == sf::Socket::NotReady)
		{
			//continue;
		}

		else if (status_server == sf::Socket::Done)
		{
			InputMemoryBitStream imbs(buffer, size * 8);
			return imbs;
		}
		else if (status_server == sf::Socket::Disconnected)
		{
			std::cout << "There was a disconnection." << std::endl;
		}
	}

	void send_data(sf::UdpSocket* socket_send, OutputMemoryBitStream& _ombs, unsigned short port)
	{
		sf::IpAddress sIp = sf::IpAddress::getLocalAddress();
		if (socket_send->send(_ombs.GetBufferPtr(), _ombs.GetByteLength(), sIp, port) != sf::Socket::Done)
		{
			std::cout << "There was an error while sending" << std::endl;
		}
	}

	void send_data(sf::UdpSocket* socket_send, std::string message, unsigned short port)
	{
		sf::IpAddress sIp = sf::IpAddress::getLocalAddress();
		char buffer[1000];
		strncpy_s(buffer, message.c_str(), sizeof(buffer));
		buffer[sizeof(buffer) - 1] = 0;
		if (socket_send->send(buffer, sizeof(buffer), sIp, port) != sf::Socket::Done)
		{
			std::cout << "There was an error while sending" << std::endl;
		}
	}
};


class Player
{
private:

public:
	int id;
	int position_x;
	int position_y;

	int old_position_x;
	int old_position_y;

	std::vector<int> desired_x;
	std::vector<int> desired_y;
};

class MovementMessage
{
private:

public:
	int id;
	int position_x;
	int position_y;
	std::string message;

	MovementMessage(int _id, int _position_x, int _position_y, std::string _message)
	{
		id = _id;
		position_x = _position_x;
		position_y = _position_y;
		message = _message;
	}

	MovementMessage()
	{
		this->id = 0;
		this->position_x = 0;
		this->position_y = 0;
		this->message = "";
	}

};



int main()
{

#pragma region connection

	/*sf::Vector2f grid[10][10];
	for (int j = 0; j < 10; j++) {
	for (int i = 0; i < 10; i++) {
	grid[i][j] = sf::Vector2f(59.5f / 2 + 59.5f*i, 59.5f / 2 + 59.5f*j);

	}
	}
	int estadoGrid[10][10];
	//0 Nada
	//1 JugadorLocal
	//2 JugadorExterno
	for (int j = 0; j < 10; j++) {
	for (int i = 0; i < 10; i++) {
	estadoGrid[i][j] = 0;

	}
	}
	/*int seed = static_cast<int>(time(0));
	srand(seed);
	int initial_x = rand() % 10;
	int initial_y = rand() % 10;
	estadoGrid[initial_x][initial_y] = 1;*/
	sf::UdpSocket socket_client;
	NetworkManager networkmanager;
	int16_t player_id = 0;
	timer timer1;
	socket_client.setBlocking(false);
	std::vector<Player> player_list;
	Player auxiliar_player;
	player_list.push_back(auxiliar_player);

	std::vector <MovementMessage> Movement_Messages_Vector;
	int16_t Packet_ID = 0;

	int delta_x = 0;
	int delta_y = 0;

	timer timermovement;
	timer movement_ack_timer;


#pragma endregion

	timer1.start();

	while (player_id == 0)
	{
		if (timer1.elapsedTime() > 0.4f)
		{


			OutputMemoryBitStream ombs;
			ombs.Write(PacketType::HELLO, PACKET_BITS);
			networkmanager.send_data(&socket_client, ombs, 5000);
			timer1.start();

		}

		sf::String mensaje_2;
		sf::IpAddress rIp;
		unsigned short port;
		std::size_t size;



		InputMemoryBitStream imbs = networkmanager.receive_data(&socket_client, &rIp, &port);

		PacketType pt = PacketType::PT_EMPTY;
		if (imbs.GetRemainingBitCount() > 0)
			imbs.Read(&pt, PACKET_BITS);

		if (pt == PacketType::WELCOME)
		{
			imbs.Read(&player_id, PLAYER_ID_BITS);
			int16_t i;
			int16_t j;
			imbs.Read(&i, GRID_X_BITS);
			imbs.Read(&j, GRID_Y_BITS);
			player_list[0].position_x = i;
			player_list[0].position_y = j;
			player_list[0].old_position_x = i;
			player_list[0].old_position_y = j;
			player_list[0].id = player_id;
		}
	}

#pragma region chat

	sf::Vector2i screenDimensions(800, 600);

	sf::RenderWindow window;
	window.create(sf::VideoMode(screenDimensions.x, screenDimensions.y), "Game");

	sf::Font font;
	if (!font.loadFromFile("courbd.ttf"))
	{
		std::cout << "Can't load the font file" << std::endl;
	}





	sf::RectangleShape temporal(sf::Vector2f(600, 5));
	sf::RectangleShape Hseparators[11];
	for (int i = 0; i < 11; i++)
	{
		Hseparators[i] = temporal;

		Hseparators[i].setFillColor(sf::Color(100, 200, 200, 255));
		Hseparators[i].setPosition(0, 59.5f * i);
	}
	sf::RectangleShape temporal2(sf::Vector2f(5, 800));
	sf::RectangleShape Vseparators[11];
	for (int i = 0; i < 11; i++)
	{
		Vseparators[i] = temporal2;

		Vseparators[i].setFillColor(sf::Color(100, 200, 200, 255));
		Vseparators[i].setPosition(59.5f * i, 0);
	}

	timermovement.start();
	movement_ack_timer.start();

	while (true)
	{
		if (shouldclose == true)
		{
			window.close();
		}


		//Send test

		// We receive
		sf::IpAddress rIp;
		unsigned short port;

		InputMemoryBitStream imbs = networkmanager.receive_data(&socket_client, &rIp, &port);

		PacketType pt = PacketType::PT_EMPTY;
		if (imbs.GetRemainingBitCount() > 0)
			imbs.Read(&pt, PACKET_BITS);

		if (pt == PacketType::PING)
		{
			int k = 0;
			imbs.Read(&k, PACKET_ID_BITS);
			OutputMemoryBitStream ombs;
			ombs.Write(PacketType::ACK, PACKET_BITS);
			ombs.Write(k, PACKET_ID_BITS);
			networkmanager.send_data(&socket_client, ombs, 5000);
		}

		else if (pt == PacketType::NEWPLAYER)
		{
			int16_t i = 0;
			int16_t j = 0;
			int k = 0;
			int h = 0;
			imbs.Read(&i, GRID_X_BITS);
			imbs.Read(&j, GRID_Y_BITS);
			imbs.Read(&k, PACKET_ID_BITS);
			imbs.Read(&h, PLAYER_ID_BITS);

			bool should_add = true;
			for (int u = 0; u < player_list.size(); u++)
			{
				if (player_list[u].id == h)
				{
					should_add = false;
					break;
				}
			}
			if (should_add == true)
			{
				auxiliar_player.position_x = i;
				auxiliar_player.position_y = j;
				auxiliar_player.id = h;
				player_list.push_back(auxiliar_player);
			}

			OutputMemoryBitStream ombs;
			ombs.Write(PacketType::ACK, PACKET_BITS);
			ombs.Write(k, PACKET_ID_BITS);

			networkmanager.send_data(&socket_client, ombs, 5000);
		}

		else if (pt == PacketType::REMOVEPLAYER)
		{
			int h = 0;
			int k = 0;
			imbs.Read(&h, PLAYER_ID_BITS);
			imbs.Read(&k, PACKET_ID_BITS);
			OutputMemoryBitStream ombs;
			ombs.Write(PacketType::ACK, PACKET_BITS);
			ombs.Write(k, PACKET_ID_BITS);

			for (int u = 0; u < player_list.size(); u++)
			{
				if (player_list[u].id == h)
				{
					player_list.erase(player_list.begin() + u);
				}
			}

			networkmanager.send_data(&socket_client, ombs, 5000);
		}

		else if (pt == PacketType::ACK)
		{
			PacketType pt_auxiliar = PacketType::PT_EMPTY;
			imbs.Read(&pt_auxiliar, PACKET_BITS);
			if (pt_auxiliar == MOVEPLAYER)
			{
				movement_ack_timer.start();
				int16_t packet_id = 0;
				int final_pos_x = 0;
				int final_pos_y = 0;
				imbs.Read(&packet_id, PACKET_ID_BITS);
				imbs.Read(&final_pos_x, GRID_X_BITS);
				imbs.Read(&final_pos_y, GRID_Y_BITS);

				player_list[0].old_position_x = final_pos_x;
				player_list[0].old_position_y = final_pos_y;
				//player_list[0].position_x = final_pos_x;
				//player_list[0].position_y = final_pos_y;
				for (int i = 0; i < Movement_Messages_Vector.size(); i++)
				{
					if (packet_id == Movement_Messages_Vector[i].id)
					{
						if (final_pos_x == Movement_Messages_Vector[i].position_x && final_pos_y == Movement_Messages_Vector[i].position_y)
						{

							if (Movement_Messages_Vector.size() > 1)
								Movement_Messages_Vector.erase(Movement_Messages_Vector.begin(), Movement_Messages_Vector.begin() + i);
							else
								//Movement_Messages_Vector.clear();
								Movement_Messages_Vector.erase(Movement_Messages_Vector.begin());
							break;
						}
						else
						{
							player_list[0].position_x = final_pos_x;
							player_list[0].position_y = final_pos_y;
							player_list[0].old_position_x = final_pos_x;
							player_list[0].old_position_y = final_pos_y;
							Movement_Messages_Vector.erase(Movement_Messages_Vector.begin() + i, Movement_Messages_Vector.begin() + Movement_Messages_Vector.size());
						}
					}
				}
			}
		}

		else if (pt == PacketType::MOVEPLAYER)
		{
			int player_id = 0;
			int final_pos_x = 0;
			int final_pos_y = 0;
			int packet_ID = 0;

			imbs.Read(&player_id, PLAYER_ID_BITS);
			imbs.Read(&final_pos_x, GRID_X_BITS);
			imbs.Read(&final_pos_y, GRID_Y_BITS);
			imbs.Read(&packet_ID, PACKET_ID_BITS);

			for (int i = 0; i < player_list.size(); i++)
			{
				if (player_list[i].id == player_id)
				{
					player_list[i].desired_x.push_back(final_pos_x);
					player_list[i].desired_y.push_back(final_pos_y);
					OutputMemoryBitStream ombs;
					ombs.Write(ACK, PACKET_BITS);
					ombs.Write(packet_ID, PACKET_ID_BITS);
					networkmanager.send_data(&socket_client, ombs, 5000);
				}
			}
		}


		sf::Event evento;
		while (window.pollEvent(evento))
		{

			switch (evento.type)
			{

			case sf::Event::Closed:
				window.close();
				break;
			case sf::Event::KeyPressed:

				if (evento.key.code == sf::Keyboard::Left)
				{
					delta_x--;
					if (player_list[0].position_x + delta_x < 50)
					{
						delta_x++;
					}
					else
						player_list[0].position_x--;
				}
				else if (evento.key.code == sf::Keyboard::Right)
				{
					delta_x++;
					if (player_list[0].position_x + delta_x > WINDOWS_X - 50)
					{
						delta_x--;
					}
					else
						player_list[0].position_x++;
				}
				else if (evento.key.code == sf::Keyboard::Up)
				{
					delta_y--;
					if (player_list[0].position_y + delta_y < 50)
					{
						delta_y++;
					}
					else
						player_list[0].position_y--;
				}
				else if (evento.key.code == sf::Keyboard::Down)
				{
					delta_y++;
					if (player_list[0].position_y + delta_y > WINDOWS_Y - 50)
					{
						delta_y--;
					}
					else
						player_list[0].position_y++;
				}
				break;
			case sf::Event::TextEntered:
				break;
			}
		}

		/*
		for (int i = 0; i < 11; i++) {
		window.draw(Hseparators[i]);
		window.draw(Vseparators[i]);
		}*/
		sf::CircleShape shape(4);
		shape.setFillColor(sf::Color(255, 100, 100));
		shape.setPosition(player_list[0].position_x, player_list[0].position_y);
		window.draw(shape);

		for (int j = 1; j < player_list.size(); j++) {
			if (player_list[j].desired_x.size() > 0 && player_list[j].desired_y.size() > 0)
			{
				if (player_list[j].desired_x[0] > player_list[j].position_x)
				{
					player_list[j].position_x++;
				}
				if (player_list[j].desired_x[0] < player_list[j].position_x)
				{
					player_list[j].position_x--;
				}



				if (player_list[j].desired_y[0] > player_list[j].position_y)
				{
					player_list[j].position_y++;
				}
				if (player_list[j].desired_y[0] < player_list[j].position_y)
				{
					player_list[j].position_y--;
				}



				if (player_list[j].desired_x[0] == player_list[j].position_x && player_list[j].desired_y[0] == player_list[j].position_y)
				{
					player_list[j].desired_x.erase(player_list[j].desired_x.begin());
					player_list[j].desired_y.erase(player_list[j].desired_y.begin());
				}
			}		
			
		}


		for (int j = 1; j < player_list.size(); j++) {
			sf::CircleShape shape(4);
			shape.setFillColor(sf::Color(132, 198, 190));
			shape.setPosition(player_list[j].position_x, player_list[j].position_y);
			window.draw(shape);
		}
		window.display();
		window.clear();

		if (timermovement.elapsedTime() > 0.2f)
		{
			if (delta_x != 0 || delta_y != 0)
			{
				Packet_ID++;
				if (Packet_ID >= MAX_PACKETS_VECTOR_SIZE - 2)
					Packet_ID = 0;

				OutputMemoryBitStream ombs;
				ombs.Write(MOVEPLAYER, PACKET_BITS);
				ombs.Write(Packet_ID, PACKET_ID_BITS);
				ombs.Write(player_id, PLAYER_ID_BITS);
				if (delta_x > 0)
				{
					ombs.Write(false, 1);
				}

				else
				{
					ombs.Write(true, 1);
				}
				int absdelta_x = abs(delta_x);
				ombs.Write(absdelta_x, DELTA_X_BITS);
				if (delta_y > 0)
				{
					ombs.Write(false, 1);
				}
				else
				{
					ombs.Write(true, 1);
				}
				int absdelta_y = abs(delta_y);
				ombs.Write(absdelta_y, DELTA_Y_BITS);
				//ombs.Write(player_list[0].position_x + delta_x, GRID_X_BITS);
				//ombs.Write(player_list[0].position_y + delta_y, GRID_Y_BITS);

				networkmanager.send_data(&socket_client, ombs, 5000);

				MovementMessage auxiliar_movement_message(Packet_ID, player_list[0].old_position_x + delta_x, player_list[0].old_position_y + delta_y, std::string(ombs.GetBufferPtr()));
				/*auxiliar_movement_message.id = Packet_ID;
				auxiliar_movement_message.position_x = player_list[0].position_x + delta_x;
				auxiliar_movement_message.position_y = player_list[0].position_y + delta_y;
				auxiliar_movement_message.message = std::string(ombs.GetBufferPtr());*/
				Movement_Messages_Vector.push_back(auxiliar_movement_message);
				delta_x = 0;
				delta_y = 0;
				//messages_no_ack.push_back(Packet_ID);
				//PacketsSent[Packet_ID] = std::string(ombs.GetBufferPtr());
			}

			timermovement.start();

		}

		if (movement_ack_timer.elapsedTime() > 5.0f)
		{
			for (int i = 0; i < Movement_Messages_Vector.size(); i++)
			{
				networkmanager.send_data(&socket_client, Movement_Messages_Vector[i].message, 5000);
			}
			movement_ack_timer.start();
		}

	}
}

#pragma endregion