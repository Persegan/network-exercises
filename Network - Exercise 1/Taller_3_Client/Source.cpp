﻿

#include <SFML\Graphics.hpp>
#include <SFML\Network.hpp>
#include <string>
#include <iostream>
#include <vector>

#define MAX_MENSAJES 30

sf::Mutex mutex;


int main()
{


#pragma region connection

	sf::Socket::Status status_client;
	sf::TcpSocket sock_client;

		do {
			status_client = sock_client.connect("127.0.0.1", 5000, sf::seconds(0.5f));
			if (status_client != sf::Socket::Done)
			{
				std::cout << "Fallo el intento de conexion: " << status_client << " en el puerto: 5000" << std::endl;
			}
			else if (status_client == sf::Socket::Done)
			{
				std::cout << "Se conecto en 5000." << std::endl;
				break;
			}
		} while (status_client != sf::Socket::Done);


		std::cout << "Se conecto al servidor." << std::endl;

		/*char data[100];
		size_t sizeRecv;
		status_client = sock_client.receive(data, 100, sizeRecv);
		std::cout << "Status: " << status_client << std::endl;
		if (status_client != sf::Socket::Done)
		{
			std::cout << "Error al recibir." << std::endl;
		}
		data[sizeRecv] = '\0';
		std::cout << "Recibo: " << data << std::endl;*/

#pragma endregion





#pragma region chat

	sock_client.setBlocking(false);

	std::vector<std::string> aMensajes;

	sf::Vector2i screenDimensions(800, 600);

	sf::RenderWindow window;
	window.create(sf::VideoMode(screenDimensions.x, screenDimensions.y), "Chat");

	sf::Font font;
	if (!font.loadFromFile("courbd.ttf"))
	{
		std::cout << "Can't load the font file" << std::endl;
	}

	sf::String mensaje = " >";

	sf::Text chattingText(mensaje, font, 14);
	chattingText.setFillColor(sf::Color(0, 160, 0));
	chattingText.setStyle(sf::Text::Bold);


	sf::Text text(mensaje, font, 14);
	text.setFillColor(sf::Color(0, 160, 0));
	text.setStyle(sf::Text::Bold);
	text.setPosition(0, 560);

	sf::RectangleShape separator(sf::Vector2f(800, 5));
	separator.setFillColor(sf::Color(200, 200, 200, 255));
	separator.setPosition(0, 550);



	while (window.isOpen())
	{
		// We receive

			char data[100];
		size_t sizeRecv;
		status_client = sock_client.receive(data, 100, sizeRecv);
		if (status_client == sf::Socket::NotReady)
		{
			//continue;
		}

		else if (status_client == sf::Socket::Done)
		{
			data[sizeRecv] = '\0';
			sf::String mensaje = data;
			aMensajes.push_back(mensaje);
			if (aMensajes.size() > 25)
			{
				aMensajes.erase(aMensajes.begin(), aMensajes.begin() + 1);
			}
		}

		else if (status_client == sf::Socket::Disconnected)
		{
			sock_client.disconnect();
			window.close();
		}


		sf::Event evento;
		while (window.pollEvent(evento))
		{

			switch (evento.type)
			{
			case sf::Event::Closed:
				sock_client.disconnect();
				window.close();
				break;
			case sf::Event::KeyPressed:
				if (evento.key.code == sf::Keyboard::Escape)
					window.close();
				else if (evento.key.code == sf::Keyboard::Return)
				{
					//We Send

						//mutex.lock();
						/*aMensajes.push_back(mensaje);
						if (aMensajes.size() > 25)
						{
							aMensajes.erase(aMensajes.begin(), aMensajes.begin() + 1);
						}*/

						std::string auxiliar_mensaje = mensaje;
						size_t bytesSent;
						status_client = sock_client.send(auxiliar_mensaje.c_str(), auxiliar_mensaje.length(), bytesSent);

						while (status_client == sf::Socket::Status::Partial)
						{
							auxiliar_mensaje = auxiliar_mensaje.substr(bytesSent, auxiliar_mensaje.length());
							status_client = sock_client.send(auxiliar_mensaje.c_str(), auxiliar_mensaje.length() - bytesSent, bytesSent);
						}

						if (status_client == sf::Socket::Status::Disconnected)
						{
							sock_client.disconnect();
							window.close();
						}
						mensaje = ">";
						//mutex.unlock();
				}
				break;
			case sf::Event::TextEntered:
				if (evento.text.unicode >= 32 && evento.text.unicode <= 126)
					mensaje += (char)evento.text.unicode;
				else if (evento.text.unicode == 8 && mensaje.getSize() > 0)
					mensaje.erase(mensaje.getSize() - 1, mensaje.getSize());
				break;
			}
		}
		window.draw(separator);
		for (size_t i = 0; i < aMensajes.size(); i++)
		{
			std::string chatting = aMensajes[i];
			chattingText.setPosition(sf::Vector2f(0, 20 * i));
			chattingText.setString(chatting);
			window.draw(chattingText);
		}
		std::string mensaje_ = mensaje + "_";
		text.setString(mensaje_);
		window.draw(text);


		window.display();
		window.clear();
	}
#pragma endregion

}

