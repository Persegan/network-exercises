#include <SFML\Graphics.hpp>
#include <SFML\Network.hpp>
#include <string>
#include <iostream>
#include <vector>



using namespace std;

int main()
{
	vector<sf::TcpSocket*> socketsListenList;

	//socketsListenList.push_back();

	// .push_back
	// .erase
	sf::Socket::Status status_server;

	sf::TcpListener listener;
	//sf::TcpSocket* incoming = new sf::TcpSocket;
	//incoming->setBlocking(false);
	status_server = listener.listen(5000);

	listener.setBlocking(false);


	std::vector<std::string> aMensajes;

	sf::String mensaje = " >";





	while (true)
	{
		//ACEPTAMOS NUEVAS CONEXIONES
		sf::TcpSocket* incoming = new sf::TcpSocket;
		incoming->setBlocking(false);

		status_server = listener.accept(*incoming);

		if (status_server == sf::Socket::Done)
		{
			socketsListenList.push_back(incoming);
		}

		else if (status_server == sf::Socket::NotReady)
		{

			delete incoming;
		}

		else
		{
			std::cout << "Error: " << status_server << " al aceptar en puerto 5000" << "." << std::endl;
		}

		//RECIBIMOS
		char data[100];
		size_t sizeRecv;
		for (int i = 0; i < socketsListenList.size(); i++) {
			status_server = socketsListenList[i]->receive(data, 100, sizeRecv);

			if (status_server == sf::Socket::NotReady)
			{
				//continue;
			}

			else if (status_server == sf::Socket::Done)
			{
				data[sizeRecv] = '\0';
				sf::String mensaje = data;
				aMensajes.push_back(mensaje);
				if (aMensajes.size() > 25)
				{
					aMensajes.erase(aMensajes.begin(), aMensajes.begin() + 1);
				}
			}

		}
		//ENVIAMOS LO RECIBIDO PUES NO MAS

		size_t bytesSent;
		for (int i = 0; i < socketsListenList.size(); i++) {
			for (int j = 0; j < aMensajes.size(); j++)
			{
				size_t bytesSent;
				std::string auxiliar_mensaje = aMensajes[j];
				status_server = socketsListenList[i]->send(auxiliar_mensaje.c_str(), auxiliar_mensaje.length(), bytesSent);

				while (status_server == sf::Socket::Status::Partial)
				{
					auxiliar_mensaje = auxiliar_mensaje.substr(bytesSent, auxiliar_mensaje.length());
					status_server = socketsListenList[i]->send(auxiliar_mensaje.c_str(), auxiliar_mensaje.length() - bytesSent, bytesSent);
				}

				if (status_server == sf::Socket::Status::Disconnected)
				{
					socketsListenList[i]->disconnect();
				}
			}
		}


		aMensajes.clear();
		
	}




}


