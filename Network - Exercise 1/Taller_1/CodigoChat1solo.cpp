﻿
///Taller 2


#include <SFML\Graphics.hpp>
#include <SFML\Network.hpp>
#include <string>
#include <iostream>
#include <vector>

#define MAX_MENSAJES 30

sf::Mutex mutex;


int main()
{


#pragma region connection
	sf::Socket::Status status_server;
	sf::TcpSocket sock_server;
	sf::Socket::Status status_client;
	sf::TcpSocket sock_client;

	char rol;
	std::cout << "Server (s) o Client (c) ... ";
	std::cin >> rol;


	if (rol == 's' || rol == 'S')
	{
		sf::TcpListener listener;

		//Hay un segundo par�metro. Ese segundo par�metro es la IP donde se aplica ese puerto. 
		//Si no se indica cada, por defecto se escucha el puerto 50000 venga a trav�s de la ip que venga
		status_server = listener.listen(5000);
		if (status_server != sf::Socket::Status::Done)
		{
			std::cout << "Error al escuchar por el puerto 5000." << std::endl;
		}



		status_server = listener.accept(sock_server);
		if (status_server != sf::Socket::Status::Done)
		{
			std::cout << "Error al aceptar conexion 1" << std::endl;
		}
		std::cout << "Llega cliente remoto de " << sock_server.getRemoteAddress() << ":" << sock_server.getRemotePort() << std::endl;
		std::cout << "Me comunico a traves del puerto " << sock_server.getLocalPort() << std::endl;

	

		std::string mensaje = "Todos conectados. Empieza la sesion!";

		status_server = sock_server.send(mensaje.c_str(), mensaje.length());
		if (status_server != sf::Socket::Status::Done)
		{
			std::cout << "Error al enviar al cliente" << std::endl;
		}

	}
	else if (rol == 'c' || rol == 'C')
	{
		do {
			status_client = sock_client.connect("127.0.0.1", 5000, sf::seconds(0.5f));
			if (status_client != sf::Socket::Done)
			{
				std::cout << "Fallo el intento de conexion: " << status_client << " en el puerto: 5000" << std::endl;
			}
			else if (status_client == sf::Socket::Done)
			{
				std::cout << "Se conecto en 5000." << std::endl;
				break;
			}
		} while (status_client != sf::Socket::Done);


		std::cout << "Se conecto al servidor." << std::endl;

		char data[100];
		size_t sizeRecv;
		status_client = sock_client.receive(data, 100, sizeRecv);
		std::cout << "Status: " << status_client << std::endl;
		if (status_client != sf::Socket::Done)
		{
			std::cout << "Error al recibir." << std::endl;
		}
		data[sizeRecv] = '\0';
		std::cout << "Recibo: " << data << std::endl;
	}

#pragma endregion





#pragma region chat

	sock_server.setBlocking(false);
	sock_client.setBlocking(false);

	std::vector<std::string> aMensajes;

	sf::Vector2i screenDimensions(800, 600);

	sf::RenderWindow window;
	window.create(sf::VideoMode(screenDimensions.x, screenDimensions.y), "Chat");

	sf::Font font;
	if (!font.loadFromFile("courbd.ttf"))
	{
		std::cout << "Can't load the font file" << std::endl;
	}

	sf::String mensaje = " >";

	sf::Text chattingText(mensaje, font, 14);
	chattingText.setFillColor(sf::Color(0, 160, 0));
	chattingText.setStyle(sf::Text::Bold);


	sf::Text text(mensaje, font, 14);
	text.setFillColor(sf::Color(0, 160, 0));
	text.setStyle(sf::Text::Bold);
	text.setPosition(0, 560);

	sf::RectangleShape separator(sf::Vector2f(800, 5));
	separator.setFillColor(sf::Color(200, 200, 200, 255));
	separator.setPosition(0, 550);



	while (window.isOpen())
	{
		sf::Event evento;
		while (window.pollEvent(evento))
		{

			//We receive
			if (rol == 'c' || rol == 'C')
			{
				char data[100];
				size_t sizeRecv;
				status_client = sock_client.receive(data, 100, sizeRecv);
				if (status_client == sf::Socket::NotReady)
				{
					continue;
				}

				else if (status_client == sf::Socket::Done)
				{
					data[sizeRecv] = '\0';
					sf::String mensaje = data;
					aMensajes.push_back(mensaje);
					if (aMensajes.size() > 25)
					{
						aMensajes.erase(aMensajes.begin(), aMensajes.begin() + 1);
					}
				}

				else if (status_client == sf::Socket::Disconnected)
				{
					sock_client.disconnect();
					window.close();
				}

				
			}

			else if (rol == 's' || rol == 'S')
			{
				char data[100];
				size_t sizeRecv;
				status_server = sock_server.receive(data, 100, sizeRecv);
				if (status_server == sf::Socket::NotReady)
				{
					continue;
				}

				else if (status_server == sf::Socket::Done)
				{
					data[sizeRecv] = '\0';
					sf::String mensaje = data;
					aMensajes.push_back(mensaje);
					if (aMensajes.size() > 25)
					{
						aMensajes.erase(aMensajes.begin(), aMensajes.begin() + 1);
					}
				}

				else if (status_server == sf::Socket::Disconnected)
				{
					sock_client.disconnect();
					window.close();
				}
			}

			switch (evento.type)
			{
			case sf::Event::Closed:
				sock_client.disconnect();
				sock_server.disconnect();
				window.close();
				break;
			case sf::Event::KeyPressed:
				if (evento.key.code == sf::Keyboard::Escape)
					window.close();
				else if (evento.key.code == sf::Keyboard::Return)
				{
					//We Send

					if (rol == 'c' || rol == 'C')
					{
						//mutex.lock();
						aMensajes.push_back(mensaje);
						if (aMensajes.size() > 25)
						{
							aMensajes.erase(aMensajes.begin(), aMensajes.begin() + 1);
						}
					
						std::string auxiliar_mensaje = mensaje;
						size_t bytesSent;
						status_client = sock_client.send(auxiliar_mensaje.c_str(), auxiliar_mensaje.length(), bytesSent);

						while (status_client == sf::Socket::Status::Partial)
						{
							auxiliar_mensaje = auxiliar_mensaje.substr(bytesSent, auxiliar_mensaje.length());
							status_client = sock_client.send(auxiliar_mensaje.c_str(), auxiliar_mensaje.length() - bytesSent, bytesSent);
						}

						if (status_client == sf::Socket::Status::Disconnected)
						{
							sock_client.disconnect();
							window.close();
						}
						mensaje = ">";
						//mutex.unlock();
					}

					if (rol == 's' || rol == 'S')
					{
						//mutex.lock();
						aMensajes.push_back(mensaje);
						if (aMensajes.size() > 25)
						{
							aMensajes.erase(aMensajes.begin(), aMensajes.begin() + 1);
						}
						//Send
						std::string auxiliar_mensaje = mensaje;
						size_t bytesSent;
						status_server = sock_server.send(auxiliar_mensaje.c_str(), auxiliar_mensaje.length(), bytesSent);


						while (status_server == sf::Socket::Status::Partial)
						{
							auxiliar_mensaje = auxiliar_mensaje.substr(bytesSent, auxiliar_mensaje.length());
							status_server = sock_server.send(auxiliar_mensaje.c_str(), auxiliar_mensaje.length() - bytesSent, bytesSent);
						}

						if (status_client == sf::Socket::Status::Disconnected)
						{
							sock_server.disconnect();
							window.close();
						}
						mensaje = ">";
						//mutex.unlock();
					}



				}
				break;
			case sf::Event::TextEntered:
				if (evento.text.unicode >= 32 && evento.text.unicode <= 126)
					mensaje += (char)evento.text.unicode;
				else if (evento.text.unicode == 8 && mensaje.getSize() > 0)
					mensaje.erase(mensaje.getSize() - 1, mensaje.getSize());
				break;
			}
		}
		window.draw(separator);
		for (size_t i = 0; i < aMensajes.size(); i++)
		{
			std::string chatting = aMensajes[i];
			chattingText.setPosition(sf::Vector2f(0, 20 * i));
			chattingText.setString(chatting);
			window.draw(chattingText);
		}
		std::string mensaje_ = mensaje + "_";
		text.setString(mensaje_);
		window.draw(text);


		window.display();
		window.clear();
	}
#pragma endregion

}





#pragma region taller 1


/*#include <SFML\Graphics.hpp>
#include <SFML\Network.hpp>
#include <string>
#include <iostream>
#include <vector>

#define MAX_MENSAJES 30

sf::Mutex mutex;

class MyClass
{

public:
	sf::TcpSocket *sock;
	std::vector<std::string> *aMensajes;

	bool stop = false;
	void receive()

	{
		sf::Socket::Status status;
		//Receive
		char data[100];
		size_t sizeRecv;
		status = sock->receive(data, 100, sizeRecv);
		if (status != sf::Socket::Done)
		{
			std::cout << "Error al recibir." << std::endl;
			sock->disconnect();
			stop = true;
		}
		mutex.lock();
		data[sizeRecv] = '\0';
		sf::String mensaje = data;
		aMensajes->push_back(mensaje);
		if (aMensajes->size() > 25)
		{
			aMensajes->erase(aMensajes->begin(), aMensajes->begin() + 1);
		}
		mutex.unlock();
		if (stop == false)
		{
			receive();

		}
		else
		{

		}
	}



};



int main()
{


#pragma region connection
	sf::Socket::Status status_server;
	sf::TcpSocket sock1_server;
	sf::TcpSocket sock2_server;
	sf::Socket::Status status_client;
	sf::TcpSocket sock_client;
	sf::TcpSocket sock2_client;

	char rol;
	std::cout << "Server (s) o Client (c) ... ";
	std::cin >> rol;


	if (rol == 's' || rol == 'S')
	{
		sf::TcpListener listener;
		sf::TcpListener listener2;

		//Hay un segundo par�metro. Ese segundo par�metro es la IP donde se aplica ese puerto. 
		//Si no se indica cada, por defecto se escucha el puerto 50000 venga a trav�s de la ip que venga
		status_server = listener.listen(5000);
		if (status_server != sf::Socket::Status::Done)
		{
			std::cout << "Error al escuchar por el puerto 5000." << std::endl;
		}

		status_server = listener2.listen(5001);
		if (status_server != sf::Socket::Status::Done)
		{
			std::cout << "Error al escuchar por el puerto 5000." << std::endl;
		}


		status_server = listener.accept(sock1_server);
		if (status_server != sf::Socket::Status::Done)
		{
			std::cout << "Error al aceptar conexion 1" << std::endl;
		}
		std::cout << "Llega cliente remoto de " << sock1_server.getRemoteAddress() << ":" << sock1_server.getRemotePort() << std::endl;
		std::cout << "Me comunico a traves del puerto " << sock1_server.getLocalPort() << std::endl;

		status_server = listener2.accept(sock2_server);
		if (status_server != sf::Socket::Status::Done)
		{
			std::cout << "Error al aceptar conexion 2" << std::endl;
		}
		std::cout << "Llega cliente remoto de " << sock2_server.getRemoteAddress() << ":" << sock2_server.getRemotePort() << std::endl;
		std::cout << "Me comunico a traves del puerto " << sock2_server.getLocalPort() << std::endl;

		std::string mensaje = "Todos conectados. Empieza la sesion!";

		status_server = sock1_server.send(mensaje.c_str(), mensaje.length());
		if (status_server != sf::Socket::Status::Done)
		{
			std::cout << "Error al enviar al cliente 1" << std::endl;
		}

	}
	else if (rol == 'c' || rol == 'C')
	{
		do {
			status_client = sock_client.connect("127.0.0.1", 5000, sf::seconds(0.5f));
			if (status_client != sf::Socket::Done)
			{
				std::cout << "Fallo el intento de conexion: " << status_client << " en el puerto: 5000" << std::endl;
			}
			else if (status_client == sf::Socket::Done)
			{
				std::cout << "Se conecto en 5000." << std::endl;
				break;
			}
		} while (status_client != sf::Socket::Done);

		do {
			status_client = sock2_client.connect("127.0.0.1", 5001, sf::seconds(0.5f));
			if (status_client != sf::Socket::Done)
			{
				std::cout << "Fallo el intento de conexion: " << status_client << " en el puerto: 5001" << std::endl;
			}
			else if (status_client == sf::Socket::Done)
			{
				std::cout << "Se conecto en 5001." << std::endl;
				break;
			}
		} while (status_client != sf::Socket::Done);

		std::cout << "Se conecto al servidor." << std::endl;



		char data[100];
		size_t sizeRecv;
		status_client = sock_client.receive(data, 100, sizeRecv);
		std::cout << "Status: " << status_client << std::endl;
		if (status_client != sf::Socket::Done)
		{
			std::cout << "Error al recibir." << std::endl;
		}
		data[sizeRecv] = '\0';
		std::cout << "Recibo: " << data << std::endl;
	}

#pragma endregion




	
#pragma region chat
	std::vector<std::string> aMensajes;

	sf::Vector2i screenDimensions(800, 600);

	sf::RenderWindow window;
	window.create(sf::VideoMode(screenDimensions.x, screenDimensions.y), "Chat");

	sf::Font font;
	if (!font.loadFromFile("courbd.ttf"))
	{
		std::cout << "Can't load the font file" << std::endl;
	}

	sf::String mensaje = " >";

	sf::Text chattingText(mensaje, font, 14);
	chattingText.setFillColor(sf::Color(0, 160, 0));
	chattingText.setStyle(sf::Text::Bold);


	sf::Text text(mensaje, font, 14);
	text.setFillColor(sf::Color(0, 160, 0));
	text.setStyle(sf::Text::Bold);
	text.setPosition(0, 560);

	sf::RectangleShape separator(sf::Vector2f(800, 5));
	separator.setFillColor(sf::Color(200, 200, 200, 255));
	separator.setPosition(0, 550);

	MyClass object;

	if (rol == 's' || rol == 'S')
	{
		object.sock = &sock2_server;
		object.aMensajes = &aMensajes;
	}

	else if (rol == 'c' || rol == 'C')
	{
		
		object.sock = &sock_client;
		object.aMensajes = &aMensajes;
	}

	sf::Thread thread(&MyClass::receive, &object);

	thread.launch();


	while (window.isOpen())
	{
		sf::Event evento;
		while (window.pollEvent(evento))
		{
			switch (evento.type)
			{
			case sf::Event::Closed:
				sock2_client.disconnect();
				sock_client.disconnect();
				sock1_server.disconnect();
				sock2_server.disconnect();
				thread.terminate();
				window.close();
				break;
			case sf::Event::KeyPressed:
				if (evento.key.code == sf::Keyboard::Escape)
					window.close();
				else if (evento.key.code == sf::Keyboard::Return)
				{
					if (rol == 'c' || rol == 'C')
					{
						mutex.lock();
						aMensajes.push_back(mensaje);
						if (aMensajes.size() > 25)
						{
							aMensajes.erase(aMensajes.begin(), aMensajes.begin() + 1);
						}
						//Send
						std::string auxiliar_mensaje = mensaje;
						status_client = sock2_client.send(auxiliar_mensaje.c_str(), auxiliar_mensaje.length());

						if (status_client != sf::Socket::Status::Done)
						{
							std::cout << "Error al enviar al servidor" << std::endl;
							sock2_client.disconnect();
							sock_client.disconnect();
							thread.terminate();
						}
						mensaje = ">";
						mutex.unlock();
					}

					if (rol == 's' || rol == 'S')
					{
						mutex.lock();
						aMensajes.push_back(mensaje);
						if (aMensajes.size() > 25)
						{
							aMensajes.erase(aMensajes.begin(), aMensajes.begin() + 1);
						}
						//Send
						std::string auxiliar_mensaje = mensaje;
						status_server = sock1_server.send(auxiliar_mensaje.c_str(), auxiliar_mensaje.length());

						if (status_server != sf::Socket::Status::Done)
						{
							std::cout << "Error al enviar al servidor" << std::endl;
							sock1_server.disconnect();
							sock2_server.disconnect();
							thread.terminate();
						}
						mensaje = ">";
						mutex.unlock();
					}
					


				}
				break;
			case sf::Event::TextEntered:
				if (evento.text.unicode >= 32 && evento.text.unicode <= 126)
					mensaje += (char)evento.text.unicode;
				else if (evento.text.unicode == 8 && mensaje.getSize() > 0)
					mensaje.erase(mensaje.getSize() - 1, mensaje.getSize());
				break;
			}
		}
		window.draw(separator);
		for (size_t i = 0; i < aMensajes.size(); i++)
		{
			std::string chatting = aMensajes[i];
			chattingText.setPosition(sf::Vector2f(0, 20 * i));
			chattingText.setString(chatting);
			window.draw(chattingText);
		}
		std::string mensaje_ = mensaje + "_";
		text.setString(mensaje_);
		window.draw(text);


		window.display();
		window.clear();
	}
#pragma endregion
	
}*/

#pragma endregion



